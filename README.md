Threads Programming using Modern C++, 3 days
====

Welcome to this course.
The syllabus can be find at
[cxx/cxx-threads](https://www.ribomation.se/cxx/cxx-threads.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Sources to the demo programs

Usage
====

Download Sources
----

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation
    
    git clone https://gitlab.com/ribomation-courses/cxx/cxx-threads.git
    cd cxx-threads

Get the latest updates by a git pull operation

    git pull

Build Programs
----

The solutions and demo programs are all using CMake as the build tool. CMake is a cross
plattform generator tool that can generate makefiles and other build tool fiels. It is also
the project descriptor for JetBrains CLion, which is my IDE of choice for C/C++ development.

You don't have to use CLion in order to compile and run the sources. What you do need is to
have cmake, make and gcc/g++ installed. On Ubuntu, you can install it like this

    sudo apt install g++ make cmake

Inside a directory with a solution or demo, run the following commands to build the program.
This will create the executable in the build directory.

    mkdir build
    cd build
    cmake -G 'Unix Makefiles' ..
    make


Installation Instructions
====

In order to do the programming exercises of the course, you need to have access to a Modern C++ compiler running on preferably a Linux system, such as Ubuntu. Go for one of the listed solutions below.

* **Already have Linux installed on your laptop**<br/>
Then you are ready for the course. If it is not Ubuntu, then there might be some differences, but as long as you can handle it and do the translation yourself, there is no problem.
* **Already have access to a remote Linux system**<br/>
Same as above.
* **Is running Windows 10 on your laptop**<br/>
One of the biggest news of the update named "Aniversary Edition" released last summer, was that Windows 10 has support for running native Ubuntu Linux, which is called WSL (Windows Subsystem for Linux). You just have to enabled it. Follow the links below to proceed.
* **Otherwise**<br/>
You have to install VirtualBox and install Ubuntu into a VM. Follow the links below to proceed.

In addition, your need a C/C++ compiler such as GCC/G++ and some way to edit your program code, such as a decent text editor or a full blown IDE. Read more below for suggestions.


Installing Ubuntu @ VBox
----

1. Install VirtualBox (VBox)<br/>
    <https://www.virtualbox.org/wiki/Downloads>
1. Create a new virtual machine (VM) i VBox, for Ubuntu Linux<br/>
    <https://www.virtualbox.org/manual/ch03.html>
1. Download an ISO file for the latest version of Ubuntu Desktop<br/>
    <http://www.ubuntu.com/download/desktop>
1. Mount the ISO file in the virtual CD drive of your VM
1. Start the VM and run the Ubuntu installation program.
    Ensure you install to the (virtual) hard-drive.
    Set a username and password when asked to and write them down.
1. Install the VBox guest additions<br/>
    <https://www.virtualbox.org/manual/ch04.html>

Installing WSL @ Windows 10
----

1. How to Install and Use the Linux Bash Shell on Windows 10<br/>
    <https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/>
1. Once successfully installed WSL, you perhaps also want to have a handy right-click menu item of "Ubuntu Here"? Here is how to do it: 
    <http://winaero.com/blog/add-bash-to-the-folder-context-menu-in-windows-10/>
1. If you would like to launch graphical applications from WSL, i.e. X-Windows apps, then you need to install a X server for Windows, such as VcXsrv, Cygwin-X or similar. Inside WSL in your ~/.bashrc file, you need to define the DISPLAY environment variable:
    
    export DISPLAY=:0

* Windows 10's Bash shell can run graphical Linux applications with this trick<br/>
    <http://www.pcworld.com/article/3055403/windows/windows-10s-bash-shell-can-run-graphical-linux-applications-with-this-trick.html>
* VcXsrv Windows X Server<br/>
    <https://sourceforge.net/projects/vcxsrv/>

Installing GCC @ Ubuntu
----

Install the GCC C/C++ compiler in Ubuntu

    sudo apt-get install g++

N.B. the sudo command will prompt you for your Ubuntu login password

Suggestions for editor/ide
----

You also need to use a text editor or IDE to write your programs. If you are already familiar with tools like Emacs or Eclipse/C++, please go ahead and install them too. On the other hand, if you would like some advise I do recommend to choose one of these two suggestions:

* If you just want a decent text editor, then go for Text Editor (gedit). It's already installed on Ubuntu and you can launch from the start menu in the upper left corner or from a terminal window with the command: `gedit my-file.cpp &`

* If you want to run a good IDE instead, then download and install the trial version of JetBrains CLion from <https://www.jetbrains.com/clion/>
This is my choice of C++ IDE and I will be using it during the course.

Our favorite C++ IDE is CLion and it works very nicely with WSL; i.e. you install CLion on Windows and uses the compiler resources within WSL. 
Just follow the instructions at
* [How to Use WSL Development Environment in CLion](https://www.jetbrains.com/help/clion/how-to-use-wsl-development-environment-in-clion.html)
* [Using WSL toolchains in CLion on Windows (YouTube)](https://youtu.be/xnwoCuHeHuY)


* Another, interesting IDE/smart-editor is MS Visual Code, which exists for Linux as well. <https://code.visualstudio.com/>


***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

