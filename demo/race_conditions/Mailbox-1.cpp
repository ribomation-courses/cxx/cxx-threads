//compile: g++ --std=c++11 -fmax-errors=1 Mailbox-1.cpp -o mailbox-1 -lpthread
//run    : ./mailbox-1 -n <int>

#include <iostream>
#include <string>
#include <vector>
#include <pthread.h>

using namespace std;

class Thread {
    pthread_t id;

    static void *wrapper(void *arg) { ((Thread *) arg)->run(); return 0;}

protected:
    virtual void run() = 0;

public:
    Thread() = default;

    void start() { pthread_create(&id, 0, wrapper, this); }

    void join() { pthread_join(id, 0); }
};

class Mailbox {
    int                 payload = 0;
    pthread_mutex_t     mutex;

public:
    Mailbox() { pthread_mutex_init(&mutex, 0); }

    ~Mailbox() { pthread_mutex_destroy(&mutex); }

    void put(int x) {
        pthread_mutex_lock(&mutex);
        payload = x;
        pthread_mutex_unlock(&mutex);
    }

    int get() {
        pthread_mutex_lock(&mutex);
        int x = payload;
        pthread_mutex_unlock(&mutex);
        return x;
    }
};

class Producer : public Thread {
    int n;
    Mailbox *mb;
public:
    Producer(int _n, Mailbox *_mb) : n(_n), mb(_mb) {}

protected:
    void run() {
        for (int k = 0; k < n; ++k) mb->put(1);
        mb->put(-1);
    }
};

class Consumer : public Thread {
    Mailbox *mb;
public:
    long sum = 0, count = 0;

    Consumer(Mailbox *_mb) : mb(_mb) {}

protected:
    void run() {
        int x;
        while ((x = mb->get()) >= 0) {
            sum += x;
            count++;
        }
    }
};

int main(int numArgs, char *args[]) {
    int numMessages = 1'000;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-m") numMessages = stoi(args[++k]);
    }
    cout << "# messages: " << numMessages << endl;

    Mailbox mb;
    Producer *p = new Producer(numMessages, &mb);
    Consumer *c = new Consumer(&mb);
    c->start();
    p->start();
    p->join();
    c->join();

    cout << "sum=" << c->sum << ", count=" << c->count << endl;

    return 0;
}

