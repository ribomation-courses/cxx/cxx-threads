#pragma  once
#include <stdexcept>

namespace ribomation::memory {
    using namespace std;
    using namespace std::string_literals;
    using byte = unsigned char;

    template<unsigned capacity>
    class CircularMemory {
        constexpr static unsigned CAPACITY       = capacity;
        constexpr static unsigned MAX_BLOCK_SIZE = CAPACITY / 10;
        byte storage[CAPACITY];
        byte* nextAddress = storage;

    public:
        CircularMemory() = default;
        ~CircularMemory() = default;
        CircularMemory(const CircularMemory&) = delete;
        CircularMemory& operator =(const CircularMemory&) = delete;

        byte* alloc(unsigned size) {
            if (size > MAX_BLOCK_SIZE) {
                throw overflow_error("too large block size"s);
            }
            if ((nextAddress + size) > (storage + CAPACITY)) {
                nextAddress = storage;
            }
            byte* addr = nextAddress;
            nextAddress += size;
            return addr;
        }
    };

}
