#pragma once

#include <pthread.h>
#include "Mutex.hpp"

class Condition {
    pthread_cond_t cond;

public:
    Condition() {
        pthread_condattr_t cfg;
        pthread_condattr_init(&cfg);

        pthread_condattr_setpshared(&cfg, PTHREAD_PROCESS_SHARED);
        pthread_cond_init(&cond, &cfg);

        pthread_condattr_destroy(&cfg);
    }

    ~Condition() {
        pthread_cond_destroy(&cond);
    }

    void wait(Mutex& m) {
        pthread_cond_wait(&cond, m.native());
    }

    void notify() {
        pthread_cond_signal(&cond);
    }

    void notifyAll() {
        pthread_cond_broadcast(&cond);
    }
};


