#pragma once

#include <array>

#include "Mutex.hpp"
#include "Condition.hpp"
#include "Guard.hpp"

namespace {
    using namespace std;

    template<typename MsgType = int, int MAX = 32>
    class MessageQueue {
        array<MsgType, MAX> queue;
        const int           max = MAX;

        Mutex     exclusive;
        Condition notFull;
        Condition notEmpty;
        
        int putIx = 0;
        int getIx = 0;
        int size  = 0;

        bool isFull()  const { return size == max; }
        bool isEmpty() const { return size == 0; }

    public:
        MessageQueue() = default;

        void put(MsgType msg) {
            Guard g(exclusive);
            while (isFull()) notFull.wait(exclusive);

            queue[putIx] = msg;
            putIx = (putIx + 1) % max;
            size++;

            notEmpty.notifyAll();
        }

        MsgType get() {
            Guard g(exclusive);
            while (isEmpty()) notEmpty.wait(exclusive);

            MsgType msg = queue[getIx];
            getIx = (getIx + 1) % max;
            size--;

            notFull.notifyAll();
            return msg;
        }
    };
}

