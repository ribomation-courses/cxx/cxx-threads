#pragma once
#include <pthread.h>

class Mutex {
    pthread_mutex_t mutex;

public:
    Mutex() {
        pthread_mutexattr_t cfg;
        pthread_mutexattr_init(&cfg);

        pthread_mutexattr_setpshared(&cfg, PTHREAD_PROCESS_SHARED);
        pthread_mutexattr_settype(&cfg, PTHREAD_MUTEX_RECURSIVE);
        pthread_mutex_init(&mutex, &cfg);

        pthread_mutexattr_destroy(&cfg);
    }

    ~Mutex() { 
        pthread_mutex_destroy(&mutex); 
    }

    void lock() { 
        pthread_mutex_lock(&mutex); 
    }

    void unlock() { 
        pthread_mutex_unlock(&mutex); 
    }

    pthread_mutex_t* native() { 
        return &mutex; 
    }
};


