#include <iostream>
#include <sstream>
#include <string>
#include <stdexcept>
#include <sys/wait.h>

#include "Thread.hpp"
#include "MessageQueue.hpp"
#include "SharedMemory.hpp"

extern "C" void exit(int);
using namespace std;


class Producer : public Thread {
    int end;
    MessageQueue<>* out;
    bool verbose;

public:
    Producer(int end, MessageQueue<>* out, bool verbose = false)
            : Thread("Producer-" + to_string(getpid())), end{end}, out{out}, verbose{verbose} {
        start();
    }

protected:
    virtual void run() override {
        log("started");

        long     sum = 0;
        for (int k   = 1; k <= end; ++k) {
            if (verbose) log(to_string(k));
            out->put(k);
            sum += k;
        }
        out->put(-1);

        log("done: sum=" + to_string(sum));
    }
};

class Consumer : public Thread {
    MessageQueue<int>* in;
    bool verbose;

public:
    Consumer(MessageQueue<>* in, bool verbose = false)
            : Thread("Consumer-" + to_string(getpid())), in{in}, verbose{verbose} {
        start();
    }

protected:
    virtual void run() {
        log("started");

        long     sum = 0;
        for (int k   = 0; (k = in->get()) > 0;) {
            if (verbose) log(to_string(k));
            sum += k;
        }

        log("done: sum=" + to_string(sum));
    }
};


int main(int numArgs, char* args[]) {
    int  numMessages = (numArgs > 1) ? stoi(args[1]) : 100;
    bool verbose     = numMessages <= 100;

    SharedMemory shm(sizeof(MessageQueue<int>));
    auto theQueue = new (shm.allocate<MessageQueue<>>()) MessageQueue<>{};

    pid_t pid = fork();
    if (pid < 0) throw runtime_error("fork() failed");

    if (pid == 0) {
        const string name = "[Child process " + to_string(getpid()) + "] ";

        cout << name << " started\n";
        auto consumer = new Consumer(theQueue, verbose);
        consumer->join();
        cout << name << " done\n";
        exit(0);
    } else {
        const string name = "[Parent process " + to_string(getpid()) + "] ";

        cout << name << " started\n";
        auto producer = new Producer{numMessages, theQueue, verbose};
        producer->join();
        wait(nullptr);
        cout << name << " done\n";
    }

    return 0;
}

