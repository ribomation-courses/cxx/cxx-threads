#include <iostream>
#include <string>
#include <sys/wait.h>
#include <chrono>
#include "shared-memory.hxx"

using namespace std;
using namespace std::string_literals;
using namespace ribomation::shm;

using XXL = unsigned long long;

XXL fibonacci(unsigned n) {
    if (n == 0)return 0;
    if (n == 1)return 1;
    return fibonacci(n - 2) + fibonacci(n - 1);
}

int main(int argc, char** argv) {
    auto         n = (argc == 1) ? 42U : stoi(argv[1]);
    SharedMemory shm{n * sizeof(XXL)};
    XXL*         results = new (shm.allocate<XXL>(n)) XXL[n];

    if (fork() == 0) {
        using namespace chrono;
        cout << "[child] computing...\n";
        auto start = system_clock::now();
        for (auto k = 0U; k < n; ++k)
            results[k] = fibonacci(k + 1);
        auto end = system_clock::now();
        cout << "[child] computed " << n << " results in "
             << duration_cast<seconds>(end-start).count() << "s\n";
        exit(0);
    }

    wait(nullptr);
    for (auto k = 0U; k < n; ++k)
        cout << "[parent] fib(" << (k + 1) << ") = " << results[k] << endl;

    return 0;
}

