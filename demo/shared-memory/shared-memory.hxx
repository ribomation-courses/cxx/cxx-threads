#pragma once

#include <string>
#include <stdexcept>
#include <cmath>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>

using namespace std;

namespace ribomation {
    namespace shm {

        class SharedMemory {
            const string    name;
            const unsigned  size;
            const void*     begin;
            const void*     end;
            void* next;

            void sysfail(const string& func) {
                string msg = func + "() failed: " + strerror(errno);
                throw runtime_error(msg);
            }

            unsigned adjustSize(size_t size) {
                const double PS = getpagesize();
                return static_cast<unsigned>(ceil(size / PS) * PS);
            }

            string adjustName(const string& name) {
                if (name[0] != '/') {
                    const_cast<string&>(name) = '/' + name;
                }
                return name;
            }

        public:
            SharedMemory(size_t size_, const string& name_ = "/cppshm", void* startAddress = 0)
                    : name(adjustName(name_)), size(adjustSize(size_)) {
                int fd = shm_open(name.c_str(), O_RDWR | O_CREAT | O_TRUNC, 0660);
                if (fd < 0) sysfail("shm_open");

                int rc = ftruncate(fd, size);
                if (rc < 0) sysfail("ftruncate");

                void* start = mmap(startAddress, size, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
                if (start == MAP_FAILED) sysfail("mmap");
                close(fd);

                SharedMemory::begin = start;
                SharedMemory::end   = start + size;
                SharedMemory::next  = start;
            }

            ~SharedMemory() {
                munmap(const_cast<void*>(begin), size);
                shm_unlink(name.c_str());
            }

            size_t getSize() const { return size; }

            void* allocateBytes(size_t numBytes) {
                void* addr = next;
                next += numBytes;
                if (end <= next) {
                    throw overflow_error("shared-memory overflow");
                }
                return addr;
            }

            template<typename T>
            T* allocate(unsigned n = 1) {
                return reinterpret_cast<T*>(allocateBytes(n * sizeof(T)));
            }

            SharedMemory() = delete;
            SharedMemory(const SharedMemory&) = delete;
            SharedMemory& operator=(const SharedMemory&) = delete;
        };

    }
}
