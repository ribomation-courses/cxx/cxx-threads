#include <iostream>
#include <string>
#include <stdexcept>
#include <fstream>
#include <iomanip>
#include <filesystem>

using namespace std;
using namespace std::literals;
namespace fs = std::filesystem;

int main(int argc, char** argv) {
    const auto baseDir = fs::path{argc == 1 ? "." : argv[1]};
    if (!fs::is_directory(baseDir)) {
        throw invalid_argument{"not a directory: "s + baseDir.string()};
    }

    cout << "Base dir: " << baseDir << endl;
    for (auto& entry : fs::recursive_directory_iterator(baseDir)) {
        auto filename = entry.path().string();
        if (fs::is_regular_file(entry)) {
            auto lastModif = fs::last_write_time(entry);
            auto ts        = decltype(lastModif)::clock::to_time_t(lastModif);
            cout << put_time(localtime(&ts), "%F %T") << "\t"
                 << fs::file_size(entry) << " bytes\t"
                 << filename << endl;
        } else if (fs::is_directory(entry)) {
            cout << filename << "/" << endl;
        } else {
            cout << filename << " ??" << endl;
        }
    }

    return 0;
}

