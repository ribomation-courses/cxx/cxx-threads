#include <iostream>
#include <fstream>
#include <iomanip>
#include <filesystem>
#include <string>
#include <stdexcept>
#include <future>
#include <vector>
#include <unordered_map>
#include <map>
#include <unordered_set>
#include <utility>

using namespace std;
using namespace std::literals;
namespace fs = std::filesystem;

struct Count {
    const string ext;
    const bool   text;
    unsigned     size  = 0;
    unsigned     lines = 0;

    Count(string ext, bool text) : ext{move(ext)}, text{text} {}
    Count() : Count{"", false} {}

    void update(const fs::path& file) {
        ifstream f{file};
        f.seekg(0, ios::end);
        size += f.tellg();
        if (text) {
            f.seekg(0);
            for (string line; getline(f, line);) ++lines;
        }
    }

    void update(const Count& cnt) {
        size += cnt.size;
        lines += cnt.lines;
    }

    ~Count() = default;
    Count(const Count&) = default;
    Count& operator =(const Count&) = delete;

    friend ostream& operator <<(ostream& os, const Count& cnt) {
        return os << setw(8) << cnt.ext << ": "
                  << setw(8) << cnt.size << " bytes "
                  << (cnt.text ? "("s + to_string(cnt.lines) + " lines)"s : ""s);
    }
};


using FileStats = unordered_map<string, Count>;

static const unordered_set<string> textExt = {
        ".txt", ".cxx", ".hxx", ".sh", ".xml", ".iml", ".log",
        ".md", ".java", ".json", ".html", ".hpp", ".cpp", ".c", ".h", ".bat"
};

static const unordered_set<string> ignoredExt = {
        ".internal", ".includecache", ".check_cache", ".marks", ".cbp", ".dir"
};

void update(FileStats& stats, const string& ext, const fs::path& file) {
    if (stats.count(ext) == 0) {
        stats.emplace(make_pair(ext, Count{ext, textExt.count(ext) > 0}));
    }
    stats[ext].update(file);
}

void update(FileStats& stats, const string& ext, const Count& cnt) {
    if (stats.count(ext) == 0) {
        stats.emplace(make_pair(ext, Count{ext, textExt.count(ext) > 0}));
    }
    stats[ext].update(cnt);
}

void update(FileStats& stats, const FileStats& dirStat) {
    for (const auto&[ext, s] : dirStat) {
        update(stats, ext, s);
    }
}


FileStats aggregate(const fs::path& dir) {
    FileStats                 stats;
    vector<future<FileStats>> dirStats;

    for (const auto& e : fs::directory_iterator{dir}) {
        if (fs::is_regular_file(e)) {
            auto ext = e.path().extension().string();
            update(stats, ext, e.path());
        } else if (fs::is_directory(e)) {
            dirStats.emplace_back( async(launch::async, [=]() {
                return aggregate(e.path());
            }) );
        }
    }
    for (auto& f : dirStats) update(stats, f.get());

    return stats;
}


int main(int argc, char** argv) {
    const auto baseDir = fs::path{argc == 1 ? "." : argv[1]};
    if (!fs::is_directory(baseDir)) {
        throw invalid_argument{"not a directory: "s + baseDir.string()};
    }

    cout << "Base dir: " << fs::canonical(baseDir) << endl;
    auto result = aggregate(baseDir);
    for (const auto& [ext, stats] : map{result.cbegin(), result.cend()})
        if (ignoredExt.count(ext) == 0 && stats.text)
            cout << stats << endl;

    return 0;
}


