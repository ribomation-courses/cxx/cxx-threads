#include <iostream>
#include <sstream>
#include <stdexcept>
#include "ObjectPool.hxx"

using namespace std;
using namespace ribomation::memory;
using XXL = unsigned long long;

struct Node {
    XXL payload;
    Node* next;
    Node* prev;

    Node() : next{this}, prev{this} {
    }

    Node(XXL payload) : payload{payload}, next{this}, prev{this} {
    }

    void insertBefore(Node* n) {
        this->next    = n;
        this->prev    = n->prev;
        n->prev->next = this;
        n->prev       = this;
    }

    void insertAfter(Node* n) {
        this->prev    = n;
        this->next    = n->next;
        n->next->prev = this;
        n->next       = this;
    }

    void remove() {
        this->prev->next = this->next;
        this->next->prev = this->prev;
        this->prev       = this->next = this;
    }
};

using NodePool = Pool<Node, 32>;


class Queue {
    NodePool& m;
    Node    * head = nullptr;

public:
    Queue(NodePool& m) : m{m} {
        head = new(m.alloc()) Node{};
    }

    ~Queue() {
        m.dispose(head);
    }

    Node* pushBack(XXL data) {
        Node* node = new(m.alloc()) Node{data};
        node->insertBefore(head);
        return node;
    }

    XXL popFront() {
        if (empty()) {
            throw underflow_error("empty queue");
        }
        Node* node = first();
        node->remove();
        XXL data = node->payload;
        m.dispose(node);
        return data;
    }

    Node* first() const {
        return head->next;
    }

    Node* last() const {
        return head->prev;
    }

    bool empty() const {
        return first() == head && last() == head;
    }

    struct iterator {
        Node* current;

        iterator(Node* n) : current(n) {}

        bool operator!=(const iterator& other) {
            return current != other.current;
        }

        iterator& operator++() {
            current = current->next;
            return *this;
        }

        Node* operator*() {
            return current;
        }
    };

    iterator begin() const {
        return {first()};
    }

    iterator end() const {
        return {head};
    }

};


void populate(Queue& q, unsigned n) {
    static unsigned next = 1;
    for (auto       k    = 1U; k <= n; ++k) {
        q.pushBack(next++);
    }
}

void drain(Queue& q, unsigned n) {
    for (auto k = 1U; k <= n && !q.empty(); ++k) {
        q.popFront();
    }
}

void drain(Queue& q) {
    while (!q.empty()) {
        q.popFront();
    }
}

void print(Queue& q) {
    for (auto x : q) cout << x->payload << " ";
    cout << endl;
}


void base() {
    NodePool m;
    cout << "m.free=" << m.free() << ", m.used=" << m.size() << endl;
    {
        Queue q{m};

        cout << "m.free=" << m.free() << ", m.used=" << m.size() << endl;
        populate(q, 30);
        print(q);

        drain(q, 15);
        populate(q, 16);
        print(q);
        cout << "m.free=" << m.free() << ", m.used=" << m.size() << endl;

        drain(q);
        populate(q, 10);
        print(q);
        populate(q, 10);
        print(q);

        drain(q, 20);
        print(q);

        drain(q);
        cout << "m.free=" << m.free() << ", m.used=" << m.size() << endl;
    }
    cout << "m.free=" << m.free() << ", m.used=" << m.size() << endl;
}

int main() {
    base();
    return 0;
}
