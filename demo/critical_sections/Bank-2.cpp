//compile: g++ --std=c++11 Bank-2.cpp -o bank-2 -lpthread
//run    : ./bank-2 -u <int> -n <int>

#include <iostream>
#include <string>
#include <vector>
#include <pthread.h>
using namespace std;


class Thread {
    pthread_t  id;
    static void*  wrapper(void* arg) {
        ((Thread*)arg)->run();
        return 0;
    }

  protected:
    virtual void run() = 0;

  public:
    Thread() = default;  
    void start() {
        pthread_create(&id, 0, wrapper, this);
    }
    void join() {pthread_join(id, 0);}
};

class Account {
    int     balance = 0;

  public:
    Account() = default;
    int getBalance() const {return balance;}
    void setBalance(int _balance) {balance = _balance;}
};

pthread_mutex_t     synchronized;

class Updater : public Thread {
    int n;
    Account* account;

  protected:
    void run() {
        for (int k=0; k<n; ++k) {
          pthread_mutex_lock(&synchronized);  
            int b = account->getBalance();
            b += 100;
            account->setBalance(b);
          pthread_mutex_unlock(&synchronized);  
        }
        for (int k=0; k<n; ++k) {
          pthread_mutex_lock(&synchronized);
            int b = account->getBalance();
            b -= 100;
            account->setBalance(b);
          pthread_mutex_unlock(&synchronized);  
        }
    }

  public:
    Updater(int _n, Account* _account) : n(_n), account(_account) {
        start();
    }
};


int main(int numArgs, char* args[]) {
    int  numUpdaters = 20;
    int  numTransactions = 1'000'000;
    
    for (int k=1; k<numArgs; ++k) {
        string arg = args[k];
        if (arg == "-u") numUpdaters = stoi(args[++k]);
        else if (arg == "-n") numTransactions = stoi(args[++k]);
    }
    
    pthread_mutex_init(&synchronized, 0);
    Account account;
    vector<Updater*> updaters;
    for (int k=0; k<numUpdaters; ++k) 
        updaters.push_back( new Updater(numTransactions, &account) );
    for (auto u : updaters) u->join();
    pthread_mutex_destroy(&synchronized);
    
    cout << "Final balance = " << account.getBalance() << endl;

    return 0;
}

