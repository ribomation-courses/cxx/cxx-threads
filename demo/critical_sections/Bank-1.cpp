//compile: g++ --std=c++11 Bank-1.cpp -o bank-1 -lpthread
//run    : ./bank-1 -u <int> -n <int>

#include <iostream>
#include <string>
#include <vector>
#include <pthread.h>
using namespace std;


class Thread {
    pthread_t  id;
    static void*  wrapper(void* arg) {
        ((Thread*)arg)->run();
        return 0;
    }

  protected:
    virtual void run() = 0;

  public:
    Thread() = default;  
    void start() {
        pthread_create(&id, 0, wrapper, this);
    }
    void join() {pthread_join(id, 0);}
};

class Account {
    int     balance = 0;
  public:
    Account() = default;
    int getBalance() const {return balance;}
    void setBalance(int _balance) {balance = _balance;}
};

class Updater : public Thread {
    int n;
    Account* account;

  protected:
    void run() {
        for (int k=0; k<n; ++k) {
            int b = account->getBalance();
            b += 100;
            account->setBalance(b);
        }
        for (int k=0; k<n; ++k) {
            int b = account->getBalance();
            b -= 100;
            account->setBalance(b);
        }
    } 
  public:
    Updater(int _n, Account* _account) : n(_n), account(_account) {
        start();
    }
};


int main(int numArgs, char* args[]) {
    int  numUpdaters = 10;
    int  numTransactions = 100'000;
    
    for (int k=1; k<numArgs; ++k) {
        string arg = args[k];
        if (arg == "-u") numUpdaters = stoi(args[++k]);
        else if (arg == "-n") numTransactions = stoi(args[++k]);
    }
    
    Account account;
    vector<Updater*> updaters;
    for (int k=0; k<numUpdaters; ++k) 
        updaters.push_back( new Updater(numTransactions, &account) );
    for (auto u : updaters) u->join();
    
    cout << "Final balance = " << account.getBalance() << endl;

    return 0;
}

