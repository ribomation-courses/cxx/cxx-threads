#pragma once
#include <iostream>
#include <sstream>
#include <string>
#include "Thread.hxx"
#include "Account.hxx"

using namespace std;
using namespace std::string_literals;

class Updater : public cxx_threads::Thread {
    const string   name;
    const unsigned numTransactions;
    const unsigned amount;
    const bool     verbose;
    Account& theAccount;

public:
    Updater(unsigned id, unsigned numTransactions, unsigned amount, bool verbose, Account& theAccount) :
            name{"updater-"s + to_string(id)},
            numTransactions{numTransactions},
            amount{amount},
            verbose{verbose},
            theAccount{theAccount} {}

    ~Updater() = default;

protected:
    void run() override {
        if (verbose) cout << (name + " started\n");
        for (auto k = 0U; k < numTransactions; ++k) theAccount.update(+amount);
        for (auto k = 0U; k < numTransactions; ++k) theAccount.update(-amount);
        if (verbose) cout << (name + " done\n");
    }
};

