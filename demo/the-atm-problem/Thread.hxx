#pragma once

#include <pthread.h>

namespace cxx_threads {
    
    class Thread {
        pthread_t thrId;

        static void* body(void* arg) {
            auto* self = reinterpret_cast<Thread*>(arg);
            self->run();
            return nullptr;
        }

    protected:
        virtual void run() = 0;

    public:
        Thread() = default;
        virtual ~Thread() = default;
        Thread(const Thread&) = delete;
        Thread& operator=(const Thread&) = delete;

        void start() {
            pthread_create(&thrId, nullptr, &body, this);
        }

        void join() {
            pthread_join(thrId, nullptr);
        }
    };

}
