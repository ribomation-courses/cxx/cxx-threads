#pragma once

class Account {
    int balance = 0;

public:
    Account() = default;
    virtual ~Account() = default;
    Account(const Account&) = delete;
    Account& operator=(const Account&) = delete;

    virtual void update(int amount) {
        int b = balance;    //READ
        b += amount;        //MODIFY
        balance = b;        //WRITE
    }

    int getBalance() const {
        return balance;
    }
};

