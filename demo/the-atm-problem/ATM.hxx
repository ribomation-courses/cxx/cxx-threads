#pragma once
#include <iostream>
#include <locale>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include "Account.hxx"
#include "Updater.hxx"

using namespace std;
using namespace std::string_literals;

template<typename Account>
struct ATM {
    unsigned numUpdaters     = 10;
    unsigned numTransactions = 100'000;
    unsigned amount          = 100;
    bool     verbose         = true;
    Account* theAccount;
    vector<Updater*> updaters;

    void run(int n, char* args[]) {
        cout.imbue(locale("en_US.UTF8"));
        parseArgs(n, args);
        setup();
        launch();
        finalBalance();
        dispose();
    }

    Account* createAccount() {
        return new Account{};
    }

    void parseArgs(int n, char* args[]) {
        for (auto k = 1; k < n; ++k) {
            string arg = args[k];
            if (arg == "-u") numUpdaters = static_cast<unsigned>(stoi(args[++k]));
            else if (arg == "-t") numTransactions = static_cast<unsigned>(stoi(args[++k]));
            else if (arg == "-a") amount = static_cast<unsigned>(stoi(args[++k]));
            else if (arg == "-v") verbose = true;
            else if (arg == "-q") verbose = false;
            else {
                cerr << "usage: " << args[0] << " [-u <num updaters>] [-t <num transactions>] [-a <amount>] \n";
                ::exit(1);
            }
        }
        if (verbose) {
            cout << "# updaters    : " << numUpdaters << endl;
            cout << "# transactions: " << numTransactions << endl;
            cout << "amount        : " << amount << endl;
        } else {
            cout << "params: " << numUpdaters << " / " << numTransactions << endl;
        }
    }

    void setup() {
        theAccount  = createAccount();
        for (auto k = 0U; k < numUpdaters; ++k)
            updaters.push_back(new Updater{k + 1, numTransactions, amount, verbose, *theAccount});
    }

    void launch() {
        for (auto u : updaters) u->start();
        for (auto u : updaters) u->join();
    }

    void finalBalance() {
        cout << "Final balance = " << theAccount->getBalance() << endl;
    }

    void dispose() {
        for (auto u : updaters) delete u;
        delete theAccount;
    }
};



