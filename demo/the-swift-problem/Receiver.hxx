#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include "Thread.hxx"
#include "MoneyBox.hxx"

using namespace std;
using namespace std::string_literals;
using namespace cxx_threads;

class ReceivingBank : public Thread {
    MoneyBox& in;
    const int stop;
    int       count = 0;
    int       total = 0;

public:
    ReceivingBank(MoneyBox& in, int stop = -1) :
            in{in},
            stop{stop} {}

    int getCount() const { return count; }
    int getTotal() const { return total; }

protected:
    void run() override {
        for (int amount; (amount = in.recv()) != stop;) {
            total += amount;
            ++count;
        }
    }
};


