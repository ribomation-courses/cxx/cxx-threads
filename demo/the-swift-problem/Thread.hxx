#pragma once

#include <pthread.h>

namespace cxx_threads {
    
    class Thread {
        pthread_t thrId;

        static void* body(void* arg) {
            Thread* self = reinterpret_cast<Thread*>(arg);
            self->run();
            return NULL;
        }

    protected:
        virtual void run() = 0;

    public:
        Thread() = default;
        virtual ~Thread() = default;

        Thread(const Thread&) = delete;
        Thread& operator=(const Thread&) = delete;

        void start() {
            pthread_create(&thrId, NULL, &body, this);
        }

        void join() {
            pthread_join(thrId, NULL);
        }
    };

}
