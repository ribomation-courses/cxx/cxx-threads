#pragma once

#include "Mutex.hxx"

using namespace cxx_threads;

class MoneyBox {
protected:
    int   payload = 0;
    Mutex mutex;

public:
    MoneyBox() = default;
    virtual ~MoneyBox() = default;
    MoneyBox(const MoneyBox&) = delete;
    MoneyBox& operator=(const MoneyBox&) = delete;

    virtual void send(int amount) {
        Guard g{mutex};
        payload = amount;
    }

    virtual int recv() {
        Guard g{mutex};
        return payload;
    }
};

