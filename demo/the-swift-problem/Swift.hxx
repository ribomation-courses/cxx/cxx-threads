#pragma once

#include "Sender.hxx"
#include "Receiver.hxx"

template<typename MoneyBox>
struct SWIFT {
    MoneyBox* theBox;
    unsigned numTransactions = 100'000;
    int      amount          = 1;
    bool     verbose         = true;

    void run(int n, char* args[]) {
        cout.imbue(locale("en_US.UTF8"));
        parseArgs(n, args);
        setup();
        launch();
        dispose();
    }

    MoneyBox* createBox() {
        return new MoneyBox{};
    }

    void parseArgs(int n, char* args[]) {
        for (auto k = 1; k < n; ++k) {
            string arg = args[k];
            if (arg == "-t") numTransactions = static_cast<unsigned>(stoi(args[++k]));
            else if (arg == "-a") amount = static_cast<unsigned>(stoi(args[++k]));
            else if (arg == "-v") verbose = true;
            else if (arg == "-q") verbose = false;
            else {
                cerr << "usage: " << args[0] << " [-u <num updaters>] [-t <num transactions>] [-a <amount>] \n";
                ::exit(1);
            }
        }
        if (verbose) {
            cout << "# transactions: " << numTransactions << endl;
            cout << "amount        : " << amount << endl;
        } else {
            cout << "params: " << numTransactions << endl;
        }
    }

    void setup() { theBox = createBox(); }

    void launch() {
        SendingBank   sender{numTransactions, *theBox, amount};
        ReceivingBank receiver{*theBox};

        receiver.start();
        sender.start();
        sender.join();
        receiver.join();

        cout << "sender  : total=" << sender.getTotal() << endl;
        cout << "receiver: total=" << receiver.getTotal() << ", count=" << receiver.getCount() << endl;
    }

    void dispose() { delete theBox; }
};


