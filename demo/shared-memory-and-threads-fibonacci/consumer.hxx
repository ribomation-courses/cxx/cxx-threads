#pragma once

#include <string>
#include <random>
#include "thrlib/thread.hxx"
#include "message-types.hxx"
#include "memlib/message-queue.hxx"

class Consumer : public Thread {
    MessageQueue<Response*>& fromServer;
    ResponsePool           & mem;

public:
    Consumer(MessageQueue<Response*>& q, ResponsePool& pool)
            : Thread{"Consumer"}, fromServer{q}, mem{pool} {
        start();
    }

protected:
    void run() override {
        using namespace std;
        using namespace std::string_literals;
        bool running;
        auto count = 1U;
        do {
            auto res = fromServer.get();
            print("["s + to_string(count++)
                  + "] fib("s + to_string(res->argument) + ") = "s + to_string(res->result)
                  + " (elapsed "s + to_string(res->elapsed()) + " ms)"s);

            running = (res->argument != STOP);
            mem.dispose(res);
        } while (running);
    }
};

