#!/usr/bin/env bash
set -e
set -x

mkdir -p build
cd build
cmake ..
cmake --build .
./fibonacci-client-server 100
