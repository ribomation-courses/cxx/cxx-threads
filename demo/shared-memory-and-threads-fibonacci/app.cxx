#include <iostream>
#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <unistd.h>
#include "memlib/shared-memory.hxx"
#include "memlib/object-pool.hxx"
#include "memlib/message-queue.hxx"
#include "message-types.hxx"
#include "producer.hxx"
#include "consumer.hxx"
#include "fibonacci-server.hxx"

using namespace std;
using namespace std::string_literals;

int main(int argc, char** argv) {
    const auto N       = (argc == 1) ? 100U : stoi(argv[1]);
    const auto shmSize = sizeof(RequestPool) + sizeof(RequestQueue)
                         + sizeof(ResponsePool) + sizeof(ResponseQueue);

    SharedMemory shm{shmSize};
    auto reqPool = new (shm.allocate<RequestPool>(1)) RequestPool{};
    auto resPool = new (shm.allocate<ResponsePool>(1)) ResponsePool{};
    auto reqQ    = new (shm.allocate<RequestQueue>(1)) RequestQueue{};
    auto resQ    = new (shm.allocate<ResponseQueue>(1)) ResponseQueue{};

    auto rc = fork();
    if (rc < 0) {
        throw runtime_error("cannot fork(): "s + strerror(errno));
    }
    if (rc == 0) {
        FibonacciServer server{*reqQ, *resQ, *reqPool, *resPool};
        server.join();
        exit(0);
    } else {
        Producer producer{N, *reqQ, *reqPool};
        Consumer consumer{*resQ, *resPool};
        producer.join();
        consumer.join();
    }

    cout << "req-pool.size = " << reqPool->size() << endl;
    cout << "res-pool.size = " << resPool->size() << endl;

    return 0;
}
