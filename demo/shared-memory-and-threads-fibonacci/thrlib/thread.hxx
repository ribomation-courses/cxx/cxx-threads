#pragma once

#include <sstream>
#include <string>
#include <pthread.h>


class Thread {
    const std::string name;
    pthread_t    thrId;

    static void* runWrapper(void* arg) {
        Thread* self = (Thread*) arg;
        self->run();
        return nullptr;
    }

protected:
    virtual void run() = 0;

    const std::string& getName() const {
        return name;
    }

    void print(const std::string& what) {
        std::ostringstream buf;
        buf << name << what << "\n";
        std::cout << buf.str();
    }

public:
    Thread(const std::string& name) : name("[" + name + "] ") {}

    virtual void start() final {
        pthread_create(&thrId, NULL, runWrapper, this);
    }

    virtual void join() final {
        pthread_join(thrId, NULL);
    }

    virtual ~Thread() = default;
    Thread() = delete;
    Thread(const Thread&) = delete;
    Thread& operator=(const Thread&) = delete;
};

