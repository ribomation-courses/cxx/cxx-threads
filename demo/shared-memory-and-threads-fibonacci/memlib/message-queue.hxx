#pragma once

#include <array>

#include "../thrlib/mutex.hxx"
#include "../thrlib/condition.hxx"
#include "../thrlib/guard.hxx"

template<typename MsgType>
class MessageQueue {
    constexpr static unsigned max = 16;
    std::array<MsgType, max>  queue;

    Mutex     exclusive;
    Condition notFull{exclusive};
    Condition notEmpty{exclusive};

    unsigned putIx = 0;
    unsigned getIx = 0;
    unsigned size  = 0;

    bool full() const { return size == max; }
    bool empty() const { return size == 0; }

public:
    MessageQueue() {}

    void put(MsgType msg) {
        Guard g{exclusive};
        notFull.waitUntil([this]() { return !full(); });

        queue[putIx] = msg;
        putIx = (putIx + 1) % max;
        ++size;

        notEmpty.notifyAll();
    }

    MsgType get() {
        Guard g{exclusive};
        notEmpty.waitUntil([this]() { return !empty(); });

        MsgType msg = queue[getIx];
        getIx = (getIx + 1) % max;
        --size;

        notFull.notifyAll();
        return msg;
    }

    MessageQueue(const MessageQueue<MsgType>&) = delete;
    MessageQueue& operator=(const MessageQueue<MsgType>&) = delete;
};

