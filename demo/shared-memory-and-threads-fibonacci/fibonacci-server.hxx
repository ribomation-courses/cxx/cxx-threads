#pragma once

#include <string>
#include "thrlib/thread.hxx"
#include "message-types.hxx"
#include "memlib/message-queue.hxx"
#include "memlib/object-pool.hxx"

class FibonacciServer : public Thread {
    RequestQueue & input;
    ResponseQueue& output;
    RequestPool& memIn;
    ResponsePool& memOut;

    static XXL fib(unsigned n) {
        if (n == 0) return 0;
        if (n == 1) return 1;
        return fib(n - 2) + fib(n - 1);
    }

public:
    FibonacciServer(RequestQueue& in, ResponseQueue& out, RequestPool& memIn, ResponsePool& memOut)
            : Thread{"FibonacciServer"}, input{in}, output{out}, memIn{memIn}, memOut{memOut} {
        start();
    }

protected:
    void run() override {
        using namespace std;
        using namespace std::string_literals;
        bool running;
        do {
            auto req    = input.get();
            auto result = fib(req->argument);
            print("fib("s + to_string(req->argument) + ") = "s + to_string(result));

            Response* res = new (memOut.alloc()) Response{req->argument, result, req->startTime};
            output.put(res);
            running = (req->argument != STOP);
            memIn.dispose(req);
        } while (running);
    }
};

