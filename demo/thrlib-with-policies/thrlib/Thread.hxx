#pragma once

#include <pthread.h>

namespace cxxThreads {

    class Thread {
        pthread_t thrId;

        static void* body(void* arg) {
            Thread* self = reinterpret_cast<Thread*>(arg);
            self->run();
            return nullptr;
        }

    protected:
        virtual void run() = 0;

    public:
        void start() {
            pthread_create(&thrId, nullptr, &body, this);
        }

        void join() {
            pthread_join(thrId, nullptr);
        }

        Thread() = default;
        virtual ~Thread() = default;
        Thread(const Thread&) = delete;
        Thread& operator =(const Thread&) = delete;
    };

}
