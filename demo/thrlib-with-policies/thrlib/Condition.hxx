#pragma once

#include <functional>
#include <pthread.h>
#include "Mutex.hxx"

namespace cxxThreads {

    struct NoopConditionPolicy {
        static void config(pthread_condattr_t*) {}
    };

    struct SharedMemoryConditionPolicy {
        static void config(pthread_condattr_t* attrs) {
            pthread_condattr_setpshared(attrs, PTHREAD_PROCESS_SHARED);
        }
    };

    template<typename MutexType, typename ...Policy>
    class Condition {
        MutexType& mutex;
        pthread_cond_t condition;

    public:
        Condition(MutexType& mutex) : mutex(mutex) {
            pthread_condattr_t attrs{};
            pthread_condattr_init(&attrs);

            (Policy::config(&attrs), ...);
            pthread_cond_init(&condition, &attrs);
            pthread_condattr_destroy(&attrs);
        }

        ~Condition() {
            pthread_cond_destroy(&condition);
        }

        void wait() {
            pthread_cond_wait(&condition, mutex.native());
        }

        void waitUntil(std::function<bool()> ready) {
            while (!ready()) wait();
        }

        void notifyOne() {
            pthread_cond_signal(&condition);
        }

        void notifyAll() {
            pthread_cond_broadcast(&condition);
        }

        MutexType& getMutex() {
            return mutex;
        }

        Condition() = delete;
        Condition(const Condition&) = delete;
    };

    template<typename ConditionType, typename PredicateType, typename StatementType>
    void guarded(ConditionType& waitCond, ConditionType& notifyCond, PredicateType&& ready, StatementType&& stmts) {
        Guard<decltype(waitCond.getMutex())> g{waitCond.getMutex()};
        waitCond.waitUntil([&] { return ready(); });
        stmts();
        notifyCond.notifyAll();
    }


}
