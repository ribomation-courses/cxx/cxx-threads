#pragma once

#include <chrono>
#include <stdexcept>
#include <cerrno>
#include <ctime>
#include <cstring>
#include <pthread.h>

namespace cxxThreads {
    using namespace std;
    using namespace std::literals;
    using namespace std::chrono;

    struct NoopMutexPolicy {
        static void config(pthread_mutexattr_t*) {}
    };

    struct RecursiveMutexPolicy {
        static void config(pthread_mutexattr_t* attrs) {
            pthread_mutexattr_settype(attrs, PTHREAD_MUTEX_RECURSIVE);
        }
    };

    struct SharedMemoryMutexPolicy {
        static void config(pthread_mutexattr_t* attrs) {
            pthread_mutexattr_setpshared(attrs, PTHREAD_PROCESS_SHARED);
        }
    };


    template<typename ...Policy>
    class Mutex {
        pthread_mutex_t mutex;

    public:
        Mutex() {
            pthread_mutexattr_t attrs;
            pthread_mutexattr_init(&attrs);

            (Policy::config(&attrs), ...);
            pthread_mutex_init(&mutex, &attrs);

            pthread_mutexattr_destroy(&attrs);
        }

        ~Mutex() {
            pthread_mutex_destroy(&mutex);
        }

        void lock() {
            pthread_mutex_lock(&mutex);
        }

        template<typename DurationType, typename Period>
        bool lock(const duration<DurationType, Period>& maxWaitingTime) {
            auto absTime  = steady_clock::now() + maxWaitingTime;
            auto secs     = time_point_cast<seconds>(absTime);
            auto nanoSecs = duration_cast<nanoseconds>(absTime - secs);

            struct timespec timeout{};
            timeout.tv_nsec = nanoSecs.count();
            timeout.tv_sec  = secs.time_since_epoch().count();

            int rc = pthread_mutex_timedlock(&mutex, &timeout);
            if (rc == 0) return true;
            if (rc == ETIMEDOUT) return false;
            throw runtime_error{"unexpected: rc="s + to_string(rc) + ", err="s + strerror(rc)};
        }

        void unlock() {
            pthread_mutex_unlock(&mutex);
        }

        pthread_mutex_t* native() {
            return &mutex;
        }

        Mutex(const Mutex&) = delete;
        Mutex& operator =(const Mutex&) = delete;
    };


    template<typename MutexType>
    class Guard {
        MutexType& mutex{};

    public:
        Guard(MutexType& mutex) : mutex{mutex} {
            mutex.lock();
        }

        ~Guard() {
            mutex.unlock();
        }

        Guard() = delete;
        Guard(const Guard&) = delete;
        Guard& operator =(const Guard&) = delete;
    };

}
