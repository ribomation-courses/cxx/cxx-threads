cmake_minimum_required(VERSION 3.12)
project(thrlib_with_policies LANGUAGES CXX)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)

add_library(thrlib INTERFACE)
target_sources(thrlib INTERFACE
        ${CMAKE_SOURCE_DIR}/thrlib/Thread.hxx
        ${CMAKE_SOURCE_DIR}/thrlib/Mutex.hxx
        ${CMAKE_SOURCE_DIR}/thrlib/Condition.hxx
        )
target_compile_options    (thrlib INTERFACE -Wall -Wextra -Werror -Wfatal-errors)
target_include_directories(thrlib INTERFACE ./thrlib)
target_link_libraries     (thrlib INTERFACE -pthread)


add_executable       (prod-cons  prod-cons.cxx)
target_link_libraries(prod-cons PRIVATE  thrlib)

add_executable       (bank  bank.cxx)
target_link_libraries(bank PRIVATE  thrlib)

add_executable       (delay  delay.cxx)
target_link_libraries(delay PRIVATE  thrlib)


