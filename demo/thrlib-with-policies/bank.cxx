#include <iostream>
#include <string>
#include "Thread.hxx"
#include "Mutex.hxx"
#include "Condition.hxx"

using namespace std;
using namespace std::literals;
using namespace cxxThreads;

using Lock = Mutex<NoopMutexPolicy, RecursiveMutexPolicy, SharedMemoryMutexPolicy>;
using LockGuard = Guard<Lock>;

class Account {
    int  balance{0};
    Lock mutex;
public:

    int get() {
        LockGuard g{mutex};
        return balance;
    }

    int update(int amount) {
        LockGuard g{mutex};
        balance = get() + amount;
        return get();
    }

    Account() = default;
    ~Account() = default;
    Account(const Account&) = delete;
    Account& operator =(const Account&) = delete;
};


int main() {
    cout << "[main] start\n";
    auto account = Account{};
    account.update(100);
    cout << "[main] Account: " << account.get() << endl;
    return 0;
}



