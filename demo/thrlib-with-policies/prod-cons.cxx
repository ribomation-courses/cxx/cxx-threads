#include <iostream>
#include <string>
#include "Thread.hxx"
#include "Mutex.hxx"
#include "Condition.hxx"

using namespace std;
using namespace std::literals;
using namespace cxxThreads;

using SimpleMutex = Mutex<>;
using SimpleGuard = Guard<SimpleMutex>;
using SimpleCondition = Condition<SimpleMutex, NoopConditionPolicy>;

template<typename PayloadType=int>
class MailBox {
    SimpleMutex     mutex{};
    SimpleCondition notEmpty{mutex};
    SimpleCondition notFull{mutex};
    PayloadType     payload{};
    bool            full{false};

public:
    void put(PayloadType x) {
        guarded(notFull, notEmpty, [this] { return !full; }, [&x, this] {
            payload = x;
            full    = true;
        });
    }

    PayloadType get() {
        PayloadType x;
        guarded(notEmpty, notFull, [this] { return full; }, [&x, this] {
            x    = payload;
            full = false;
        });
        return x;
    }
};

struct Producer : Thread {
private:
    int N;
    MailBox<>& box;
public:
    Producer(int n, MailBox<>& box) : N{n}, box{box} {
        start();
    }

protected:
    void run() override {
        for (auto k = 1; k <= N; ++k) {
            box.put(k);
        }
        box.put(0);
    }
};

struct Transformer : Thread {
private:
    MailBox<>& in;
    MailBox<>& out;
public:
    Transformer(MailBox<>& in, MailBox<>& out) : in{in}, out{out} {
        start();
    }

protected:
    void run() override {
        for (auto k = in.get(); k > 0; k = in.get()) {
            out.put(2 * k);
        }
        out.put(0);
    }
};

struct Consumer : Thread {
private:
    MailBox<>& box;
public:
    Consumer(MailBox<>& box) : box{box} {
        start();
    }

protected:
    void run() override {
        for (auto k = box.get(); k > 0; k = box.get()) {
            cout << ("[consumer] "s + to_string(k) + "\n"s);
        }
    }
};


int main() {
    MailBox<> box[3];

    Consumer    c{box[2]};
    Transformer t2{box[1], box[2]};
    Transformer t1{box[0], box[1]};
    Producer    p{100, box[0]};

    c.join();
    return 0;
}

