#include <iostream>
#include <string>
#include <chrono>
#include <unistd.h>
#include "Thread.hxx"
#include "Mutex.hxx"

using namespace std;
using namespace std::literals;
using namespace std::chrono;
using namespace cxxThreads;

struct Holder : Thread {
private:
    Mutex<>& mx;
public:
    Holder(Mutex<>& mx) : mx{mx} {}

protected:
    void run() override {
        cout << "[Holder] start\n";
        mx.lock();
        sleep(3);
        mx.unlock();
        cout << "[Holder] done\n";
    }
};

struct Requester : Thread {
private:
    Mutex<>& mx;
public:
    Requester(Mutex<>& mx) : mx{mx} {}

protected:
    void run() override {
        cout << "[Requester] start\n";
        sleep(1);
        if (!mx.lock(4s)) {
            cout << "[Requester] timeout!!\n";
            return;
        }
        mx.unlock();
        cout << "[Requester] done\n";
    }
};


int main() {
    Mutex<> mx;
    Holder h{mx};
    Requester r{mx};
    h.start();
    r.start();
    r.join();
    h.join();
    return 0;
}

