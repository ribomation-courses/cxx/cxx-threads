#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

using namespace std;

int main() {
    vector<string> words = {"C++", "is", "really", "cooooool", "!!!"};

    cout << "words: ";
    for_each(words.begin(), words.end(), [](auto& w){
        cout << ("*" + w + "* ");
    });
    cout << endl;

    return 0;
}
