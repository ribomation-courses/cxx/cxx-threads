cmake_minimum_required(VERSION 3.8)
project(01_modern_cxx)

set(CMAKE_CXX_STANDARD 14)

set(SOURCE_FILES main.cpp)
add_executable(01_modern_cxx ${SOURCE_FILES})