#pragma once

#include <iostream>
#include <sstream>
#include <random>
#include <unistd.h>
#include "Thread.hxx"
#include "Barrier.hxx"

class Worker : public cppThreads::Thread {
    unsigned id;
    cppThreads::Barrier& synch;
    bool running = true;

    void print(const std::string& marker) {
        std::ostringstream buf;
        buf << std::string(id * 3, ' ') << marker << "\n";
        std::cout << buf.str();
    }

public:
    Worker(unsigned id, cppThreads::Barrier& b) : id(id), synch(b) {
        start();
    }

protected:
    void run() override {
        using namespace std;
        using namespace std::string_literals;
        
        default_random_engine              r;
        uniform_int_distribution<unsigned> nextValue(1, 10);

        unsigned cnt = nextValue(r);
        synch.wait();

        while (running) {
            print("."s);
            usleep(nextValue(r) * 100000);

            if (cnt-- == 0) {
                print("#"s);
                synch.wait();

                cnt = nextValue(r);
                print("+"s);
            }
        }
    }
};
