#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <random>
#include <unistd.h>
#include "Thread.hxx"
#include "Barrier.hxx"
#include "Worker.hxx"

using namespace std;
using namespace cppThreads;

int main(int numArgs, char* args[]) {
    auto numWorkers   = (numArgs == 1) ? 10U : stoi(args[1]);

    Barrier         phase(numWorkers);
    vector<Worker*> workers;
    for (unsigned   k = 0; k < numWorkers; ++k) {
        workers.push_back(new Worker(k, phase));
    }
    workers[0]->join();

    return 0;
}

