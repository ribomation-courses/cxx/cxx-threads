#pragma once

#include <pthread.h>

namespace cppThreads {
    class Barrier {
        pthread_barrier_t barrier{};

    public:
        explicit Barrier(unsigned numWorkers) {
            pthread_barrier_init(&barrier, nullptr, numWorkers);
        }
        
         ~Barrier() {
             pthread_barrier_destroy(&barrier);
         }
        
        void wait() {
            pthread_barrier_wait(&barrier);
        }
    };
}

