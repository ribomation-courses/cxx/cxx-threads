#include <iostream>
#include <string>
#include <vector>
#include "Thread.hxx"
#include "StockTicker.hxx"
#include "StockTickerUpdater.hxx"
#include "StockTickerReader.hxx"

int main(int numArgs, char* args[]) {
    using namespace std;
    using namespace cppThreads;

    auto numReaders = 50;
    auto numUpdates = 10;

    for (auto k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-r") numReaders = stoi(args[++k]);
        else if (arg == "-u") numUpdates = stoi(args[++k]);
    }

    Ticker          tick{"GOOG"};
    Updater         u{numUpdates, &tick};
    vector<Reader*> readers;
    for (auto id = 1; id <= numReaders; ++id) {
        readers.push_back(new Reader(id, &tick));
    }

    u.join();
    for (auto r : readers) { r->join(); }

    return 0;
}
