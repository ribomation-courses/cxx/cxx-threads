#pragma once

#include <pthread.h>

namespace cppThreads {
    class ReadWriteLock {
        pthread_rwlock_t rwl{};

    public:
        ReadWriteLock() {
            pthread_rwlock_init(&rwl, nullptr);
        }

        ~ReadWriteLock() {
            pthread_rwlock_destroy(&rwl);
        }

        void lockForReading() {
            pthread_rwlock_rdlock(&rwl);
        }

        void lockForWriting() {
            pthread_rwlock_wrlock(&rwl);
        }

        void unlock() {
            pthread_rwlock_unlock(&rwl);
        }
    };


    class AbstractRWLGuard {
        ReadWriteLock& rwl;

    protected:
         explicit AbstractRWLGuard(ReadWriteLock& rwl) : rwl{rwl} { }

    public:
        virtual ~AbstractRWLGuard() {
            rwl.unlock();
        }
    };

    struct LockForReading :  AbstractRWLGuard {
        explicit LockForReading(ReadWriteLock& rwl) : AbstractRWLGuard{rwl} {
            rwl.lockForReading();
        }
    };

    struct LockForWriting :  AbstractRWLGuard {
        explicit LockForWriting(ReadWriteLock& rwl) : AbstractRWLGuard{rwl} {
            rwl.lockForWriting();
        }
    };

}
