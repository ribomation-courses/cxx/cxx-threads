#pragma once

#include <iostream>
#include <random>
#include <unistd.h>
#include "Thread.hxx"
#include "StockTicker.hxx"

class Updater : public cppThreads::Thread {
    int numUpdates;
    Ticker* ticker;

public:
    Updater(int numUpdates, Ticker* ticker)
            : numUpdates(numUpdates), ticker(ticker) {
        start();
    }

protected:
    void run() override {
        using namespace std;
        default_random_engine      r;
        normal_distribution<float> nextAmount(100, 150);

        for (auto k = 0; k < numUpdates; ++k) {
            auto amt = nextAmount(r);
            
            ostringstream buf;
            buf << "**** Updater:" << amt << "\n";
            cout << buf.str();

            ticker->update(static_cast<int>(amt));
            sleep(2);
        }

        ticker->update(-99999);
        ostringstream buf;
        buf << "**** Updater: done\n";
        cout << buf.str();
    }
};

