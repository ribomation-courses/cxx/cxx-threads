#pragma once

#include <iostream>
#include <sstream>
#include <random>
#include <unistd.h>
#include "Thread.hxx"
#include "StockTicker.hxx"

class Reader : public cppThreads::Thread {
    const unsigned id;
    Ticker* ticker;
public:
    Reader(int id, Ticker* ticker) : id(id), ticker(ticker) {
        start();
    }

protected:
    void run() override {
        using namespace std;
        default_random_engine              r;
        uniform_int_distribution<unsigned> nextSleepTime(100, 500);
        do {
            ostringstream buf;
            buf << "Reader-" << id << ": " << ticker->toString() << "\n";
            cout << buf.str();

            usleep(nextSleepTime(r) * 1000);
        } while (ticker->get() > -9999);

        ostringstream buf;
        buf << "Reader-" << id << ": done\n";
        cout << buf.str();
    }
};

