#pragma once

#include <iostream>
#include <sstream>
#include "ReadWriteLock.hxx"
#include "StockTicker.hxx"

class Ticker {
    const std::string         name;
    int                       value = 0;
    cppThreads::ReadWriteLock rwl;

public:
    explicit Ticker(const std::string& name) : name(name) {}

    double get() {
        cppThreads::LockForReading g(rwl);
        return value;
    }

    void update(int amount) {
        cppThreads::LockForWriting g(rwl);
        value += amount;
    }

    std::string toString() {
        std::ostringstream buf;
        buf << name << ": " << get();
        return buf.str();
    }
};
