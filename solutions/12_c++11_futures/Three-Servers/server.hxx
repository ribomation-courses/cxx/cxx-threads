#pragma once
#include <iostream>
#include <string>
#include <functional>
#include "lib/Message.hxx"
#include "lib/MessageQueue.hxx"
#include "lib/sync-output.hxx"
using namespace std;
using namespace std::literals;


class Server {
    string                            name;
    function<long(int x)>             compute;
    MessageQueue<Message<int, long>*> inbox;

public:
    Server(string n, function<long(int x)> f) : name{move(n)}, compute{move(f)} {}

    future<long> request(Message<int, long>* m) {
        inbox.put(m);
        return m->reply.get_future();
    }

    void shutdown() {
        Message<int, long> m{-1};
        request(&m).wait_for(1s);
    }

    void run() {
        cout << (name + "started\n");
        bool running = true;
        try {
            do {
                Message<int, long>* m = inbox.get();
                {
                    SyncOut{cout} << name << "recv: " << m->message << "\n";
                }

                long result = compute(m->message);
                {
                    SyncOut{cout} << name << "rply: " << m->message << " -> " << result << "\n";
                }
                m->reply.set_value(result);
                running = (m->message >= 0);
            } while (running);
        } catch (...) { cerr << "WTF !!" << endl; }
        cout << (name + "done\n");
    }
};


