#pragma once
#include <iostream>
#include <sstream>
using namespace std;

class SyncOut : public ostringstream {
    ostream& out;
public:
    explicit SyncOut(ostream& out) : out{out} {}

    ~SyncOut() override {
        out << ostringstream::str() << std::flush;
    }
};
