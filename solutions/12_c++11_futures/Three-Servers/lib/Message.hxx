#pragma once

#include <future>

using namespace std;

template<typename MessageType, typename ReplyType>
struct Message {
    MessageType        message;
    promise<ReplyType> reply;

    explicit Message(MessageType x) : message(x) {}
};

