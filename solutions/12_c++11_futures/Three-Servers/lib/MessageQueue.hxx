#pragma  once

#include <mutex>
#include <condition_variable>
#include <queue>

using namespace std;


template<typename PayloadType>
class MessageQueue {
    queue<PayloadType> messages;
    mutex              exclusive;
    condition_variable notEmpty;

public:
    MessageQueue() = default;
    ~MessageQueue() = default;
    MessageQueue(const MessageQueue<PayloadType>&) = delete;
    MessageQueue<PayloadType>& operator=(const MessageQueue<PayloadType>&) = delete;

    void put(PayloadType msg) {
        unique_lock<mutex> guard(exclusive);
        messages.push(msg);
        notEmpty.notify_all();
    }

    PayloadType get() {
        unique_lock<mutex> guard(exclusive);
        notEmpty.wait(guard, [this] { return !messages.empty(); });
        PayloadType msg = messages.front();
        messages.pop();
        return msg;
    }
};
