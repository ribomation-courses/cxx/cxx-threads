#include <iostream>
#include <sstream>
#include <string>
#include <functional>
#include <thread>
#include <future>
#include <memory>
#include <utility>
#include "lib/Message.hxx"
#include "lib/MessageQueue.hxx"
#include "lib/sync-output.hxx"
#include "server.hxx"
using namespace std;
using namespace std::literals;

long _fib(int n) { return n <= 2 ? 1 : _fib(n - 1) + _fib(n - 2); }
long _fac(int n) { return n <= 1 ? 1 : n * _fac(n - 1); }
long _sum(int n) { return n <= 1 ? 1 : n + _sum(n - 1); }

void Client(Server* fib, Server* fac, Server* sum, unsigned N) {
    cout << ("[client] started\n");
    for (auto i = 1U; i <= N; ++i) {
        auto m1 = make_unique<Message<int, long>>(i % 45);
        auto m2 = make_unique<Message<int, long>>(i % 10);
        auto m3 = make_unique<Message<int, long> >(i % 100);

        auto f1 = fib->request(m1.get());
        auto f2 = fac->request(m2.get());
        auto f3 = sum->request(m3.get());

        SyncOut{cout}
                << "[client] waiting for replies (" << i << ")...\n"
                << "[client] fib("s << m1->message << ") = "s << f1.get() << "\n"
                << "[client] fac("s << m2->message << ") = "s << f2.get() << "\n"
                << "[client] sum("s << m3->message << ") = "s << f3.get() << "\n"
                ;
    }
    cout << "[client] done\n" << flush;
}

int main(int numArgs, char* args[]) {
    int numMessages = 100;

    for (int i = 1; i < numArgs; ++i) {
        string arg = args[i];
        if (arg == "-m") numMessages = stoi(args[++i]);
    }
    cout << "numMessages: " << numMessages << endl;

    Server fib("[fibonacci] "s, _fib);
    Server fac("[factorial] "s, _fac);
    Server sum("[sum      ] "s, _sum);

    thread fibThr(&Server::run, &fib);
    thread facThr(&Server::run, &fac);
    thread sumThr(&Server::run, &sum);

    thread client{Client, &fib, &fac, &sum, numMessages};
    client.join();

    fib.shutdown();
    fibThr.join();
    fac.shutdown();
    facThr.join();
    sum.shutdown();
    sumThr.join();

    return 0;
}
