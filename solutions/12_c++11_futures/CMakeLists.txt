cmake_minimum_required(VERSION 3.14)
project(12_Futures LANGUAGES CXX)

add_subdirectory(Three-Servers)
add_subdirectory(Async-Word-Freqs)

