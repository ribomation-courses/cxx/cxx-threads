#include <iostream>
#include <iomanip>
#include <fstream>

#include <string>
#include <string_view>
#include <stdexcept>

#include <utility>
#include <tuple>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <future>
#include <chrono>

#include <cctype>
#include <cstring>
#include <cerrno>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>

using namespace std;
using namespace std::literals;
using namespace std::chrono;
using Frequencies = unordered_map<string_view, unsigned>;
using FreqPair = pair<string_view, unsigned>;

void print(const vector<FreqPair>& words, unsigned max);
auto filesize(fstream& file) -> unsigned long;
auto load(const string& filename) -> tuple<char*, unsigned long>;
auto count(string_view payload) -> Frequencies;
auto aggregate(vector<future<Frequencies>>& partialResults) -> Frequencies;

int main(int argc, char** argv) {
    auto filename = "data/shakespeare.txt"s;
    if (argc > 1) {
        filename = argv[1];
    }

    auto startTime = high_resolution_clock::now();

    auto[content, size] = load(filename);
    transform(content, content + size, content, [](auto ch) { return tolower(ch); });

    auto maxNumWords    = 25U;
    auto numTask        = thread::hardware_concurrency();
    auto chunkSize      = size / numTask;
    auto payload        = string_view{content, size};
    auto partialResults = vector<future<Frequencies>>{};

    for (auto k        = 0U; k < numTask; ++k) {
        auto chunk = payload.substr(k * chunkSize, chunkSize);
        partialResults.push_back(async(launch::async, [=]() { return count(chunk); }));
    }
    auto      results  = aggregate(partialResults);
    auto      sortable = vector<FreqPair>{results.begin(), results.end()};
    sort(sortable.begin(), sortable.end(), [](FreqPair lhs, FreqPair rhs) {
        return lhs.second > rhs.second;
    });

    auto endTime = high_resolution_clock::now();

    print(sortable, maxNumWords);
    cout.imbue(locale{"en_US.UTF8"s});
    cout << "------\n";
    cout << "# Tasks     : " << numTask << "\n";
    cout << "File Size   : " << size << " bytes\n";
    cout << "Chunk Size  : " << chunkSize << " bytes\n";
    cout << "Elapsed Time: " << duration_cast<nanoseconds>(endTime - startTime).count() * 1E-9 << " seconds\n";

    return 0;
}


auto filesize(fstream& f) -> unsigned long {
    auto posOrig = f.tellg();
    f.seekg(0, ios::end);
    auto size = static_cast<unsigned long>(f.tellg());
    f.seekg(posOrig, ios::beg);
    return size;
}

auto load(const string& filename) -> tuple<char*, unsigned long> {
    fstream f{filename, ios::in | ios::binary};
    if (!f) throw invalid_argument{"cannot open "s + filename};

    auto size = filesize(f);
    char* payload = new char[size];
    f.read(payload, size);

    return make_tuple(payload, size);
}

auto count(string_view payload) -> Frequencies {
    Frequencies freqs;

    auto start = 0UL;
    do {
        while (!isalpha(payload[start]) && (start < payload.length())) ++start;

        auto end = start;
        while (isalpha(payload[end]) && (start < payload.length())) ++end;

        string_view word = payload.substr(start, end - start);
        if (word.size() > 3) ++freqs[word];

        start = end + 1;
    } while (start < payload.length());

    return freqs;
}

auto aggregate(vector<future<Frequencies>>& partialResults) -> Frequencies {
    Frequencies results;
    for (auto& fut : partialResults) {
        for (auto& [word, freq] : fut.get()) results[word] += freq;
    }
    return results;
}

void print(const vector<FreqPair>& words, unsigned max) {
    unsigned cnt = 0;
    for (auto& [word, freq] : words) {
        cout << word << ": " << freq << "\n";
        if (++cnt >= max) return;
    }
}

