#pragma once
#include <iosfwd>
#include <sstream>
using namespace std;
using namespace std::literals;

// Inspired by
// https://en.cppreference.com/w/cpp/io/basic_osyncstream
// which will be part of C++20
struct AtomicOstream : ostringstream {
    AtomicOstream(ostream& out) : out{out} {}
    ~AtomicOstream() {
        out << (ostringstream::str() + "\n");
    }
private:
    ostream& out;
};

