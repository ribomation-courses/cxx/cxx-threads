#pragma  once


#include <condition_variable>
#include <mutex>
#include <thread>
#include <queue>
#include <iostream>


template<typename PayloadType>
class MessageQueue {
protected:
    const unsigned          MAX = 16;
    std::queue<PayloadType> q;
    std::mutex              lock;
    std::condition_variable notFull;
    std::condition_variable notEmpty;

public:
    MessageQueue() = default;
    ~MessageQueue() = default;
    MessageQueue(const MessageQueue<PayloadType>&) = delete;
    MessageQueue<PayloadType>& operator=(const MessageQueue<PayloadType>&) = delete;

    void send(PayloadType x) {
        std::unique_lock<std::mutex> g(MessageQueue<PayloadType>::lock);
        notFull.wait(g, [&] { return q.size() < MAX; });

        q.push(x);

        notEmpty.notify_all();
    }

    PayloadType receive() {
        std::unique_lock<std::mutex> g(MessageQueue<PayloadType>::lock);
        notEmpty.wait(g, [&]() { return !q.empty(); });

        PayloadType x = q.front();
        q.pop();

        notFull.notify_all();
        return x;
    }

    PayloadType operator<<(PayloadType x) {
        send(x);
        return x;
    }

    PayloadType operator>>(PayloadType& x) {
        x = receive();
        return x;
    }
};

