#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <thread>
#include "MessageQueue.hxx"
#include "atomic-ostream.hxx"
using namespace std;


void Producer(unsigned N, MessageQueue<long>* out) {
    for (unsigned k = 1; k <= N; ++k) { *out << k; }
    *out << -1;

     AtomicOstream{cout} << "[producer] done";
}

void Transformer(unsigned id, MessageQueue<long>* in, MessageQueue<long>* out) {
    for (long msg; (*in >> msg) > 0;) { *out << 2 * msg; }
    *out << -1;

    AtomicOstream{cout} << "[transformer-" << id << "] done";
}

void Consumer(MessageQueue<long>* in) {
    for (long msg; (*in >> msg) > 0; ) {
        AtomicOstream{cout} <<"[consumer] " << msg;
    }

    AtomicOstream{cout} <<"[consumer] done";
}


bool operator<<(const string& target, const string& prefix) {
    return target.substr(0, prefix.size()) == prefix;
}

unsigned stou(const string& txt) {
    return static_cast<unsigned>(stoul(txt));
}

int main(int numArgs, char* args[]) {
    using namespace string_literals;
    const auto options               = " [--transformers={int}] [--messages={int}]"s;
    auto       numTransformers       = 10U;
    auto       numMessages           = 30U;

    for_each(args + 1, args + numArgs, [&](string arg) -> void {
        if (arg << "--transformers="s) {
            numTransformers = stou(arg.substr(15));
        } else if (arg << "--messages="s) {
            numMessages = stou(arg.substr(11));
        } else {
            cerr << "unknown option: " << arg << endl;
            cerr << "usage: " << args[0] << options << endl;
            ::exit(1);
        }
    });
    cout << "# transformers: " << numTransformers << endl;
    cout << "# messages    : " << numMessages << endl;

    vector<MessageQueue<long>> queues(numTransformers + 1);
    unsigned long              mbIdx = queues.size() - 1;

    thread   c(Consumer, &queues[mbIdx]);
    unsigned id                      = 1;
    while (numTransformers-- > 0) {
        thread t(Transformer, id++, &queues[mbIdx - 1], &queues[mbIdx]);
        t.detach();
        --mbIdx;
    }
    thread   p(Producer, numMessages, &queues[0]);
    p.join();
    c.join();

    return 0;
}
