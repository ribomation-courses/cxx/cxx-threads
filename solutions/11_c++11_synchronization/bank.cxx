#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <random>
#include <thread>
#include <mutex>
#include <tuple>
#include "atomic-ostream.hxx"

using namespace std;
using RandomIndex = uniform_int_distribution<unsigned>;

struct Account {
    recursive_mutex lock;
    int             balance = 0;
};

auto pick_two(vector<Account>& accounts, default_random_engine& r) -> tuple<Account*, Account*> {
    const auto  lb = 0U;
    const auto  ub = static_cast<unsigned>(accounts.size() - 1);
    RandomIndex nextAccount{lb, ub};

    auto fromIdx = nextAccount(r);
    auto toIdx   = 0U;
    do {
        toIdx = nextAccount(r);
    } while (fromIdx == toIdx);

    return make_tuple(&accounts[fromIdx], &accounts[toIdx]);
}

void Updater(int id, vector<Account>* accounts, int numTransactions, int amount) {
    {
        AtomicOstream{cout} << "Updater-" << id << " started";
    }
    //cout << ("Updater-" + to_string(id) + " started\n");

    random_device  seed{};
    default_random_engine r{seed()};
    for (auto             k = 0; k < numTransactions; ++k) {
        auto[from, to] = pick_two(*accounts, r);
        scoped_lock guard{from->lock, to->lock};
        
        from->balance -= amount;
        to->balance   += amount;
    }

    {
        AtomicOstream{cout} << "Updater-" << id << " done";
    }
    //cout << ("Updater-" + to_string(id) + " done\n");
}

auto totalSum(const vector<Account>& accounts) {
    return accumulate(accounts.cbegin(), accounts.cend(),
                      0,
                      [](auto sum, auto& a) { return sum + a.balance; });
}

int main(int numArgs, char* args[]) {
    auto numUpdaters     = 10;
    auto numTransactions = 1'000'000;
    auto numAccounts     = numUpdaters / 2;
    auto amount          = 100;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-u") numUpdaters = stoi(args[++k]);
        else if (arg == "-t") numTransactions = stoi(args[++k]);
        else if (arg == "-a") numAccounts = stoi(args[++k]);
        else {
            cerr << "usage: " << args[0] << " [-a num accounts] [-u <num updaters>] [-t <num transactions>]\n";
            return 1;
        }
    }

    cout.imbue(locale{"en_US.UTF-8"});
    cout << "# accounts     = " << numAccounts << endl;
    cout << "# updaters     = " << numUpdaters << endl;
    cout << "# transactions = " << numTransactions << endl;
    cout << "amount         = " << amount << endl;

    vector<Account> accounts{static_cast<unsigned long>(numAccounts)};
    vector<thread>  updaters{};
    updaters.reserve(static_cast<unsigned long>(numUpdaters));

    cout << "[before] SUM balance = " << totalSum(accounts) << endl;
    for (auto k = 0; k < numUpdaters; ++k) {
        updaters.emplace_back(&Updater, k + 1, &accounts, numTransactions, amount);
    }
    for (auto& t : updaters) t.join();
    cout << "[after]  SUM balance = " << totalSum(accounts) << endl;

    return 0;
}
