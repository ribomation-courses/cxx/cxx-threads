cmake_minimum_required(VERSION 3.10)
project(11_CXX11_Synchronization LANGUAGES CXX)

set(CMAKE_CXX_STANDARD          17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS        OFF)

add_executable(bank
        atomic-ostream.hxx
        bank.cxx )
target_link_libraries(bank  -pthread)
target_compile_options(bank PRIVATE -Wall -Wextra -Werror -Wfatal-errors)

add_executable(pipeline
        atomic-ostream.hxx
        MessageQueue.hxx
        pipeline.cxx)
target_link_libraries(pipeline  -pthread)
target_compile_options(pipeline PRIVATE -Wall -Wextra -Werror -Wfatal-errors)

