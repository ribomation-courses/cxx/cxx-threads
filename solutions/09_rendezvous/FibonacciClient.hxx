#pragma  once
#include <iostream>
#include <iomanip>
#include <sstream>
#include <chrono>
#include <random>
#include "FibonacciServer.hxx"
using namespace std;
using namespace std::chrono;
using namespace cppThreads;


class Client : public Thread {
    unsigned id;
    unsigned numArgs;
    FibonacciServer& srv;

public:
    Client(unsigned id, unsigned numArgs, FibonacciServer& srv) : id(id), numArgs(numArgs), srv(srv) {
        start();
    }

    Client(Client&& that) noexcept : id{that.id}, numArgs{that.numArgs}, srv{that.srv} {
        start();
    }

protected:
    void run() override {
        random_device                      r;
        uniform_int_distribution<unsigned> nextArg{id, 45};

        for (auto k = 0U; k < numArgs; ++k) {
            auto arg = nextArg(r);
            log(arg);

            auto startTime      = high_resolution_clock::now();
                auto result         = srv.send(arg);
            auto endTime        = high_resolution_clock::now();
            auto elapsedSeconds = duration_cast<milliseconds>(endTime - startTime).count() * 10E-3;
            log(arg, result, elapsedSeconds);
        }
    }

private:
    void log(unsigned arg) {
        ostringstream buf;
        buf << "[client-" << id << "] fib(" << arg << ") ...\n";
        cout << buf.str();
    }

    void log(unsigned arg, unsigned long result, double elapsed) {
        ostringstream buf;
        buf << "[client-" << id << "] fib(" << arg << ") = " << result
            << " (elapsed " << elapsed << " secs.)\n";
        cout << buf.str();
    }
};
