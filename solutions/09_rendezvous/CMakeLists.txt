cmake_minimum_required(VERSION 3.10)
project(08_Rendezvous LANGUAGES CXX)

set(CMAKE_CXX_STANDARD          17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS        OFF)

set(LIB
        lib/Thread.hxx
        lib/Mutex.hxx
        lib/Guard.hxx
        lib/Condition.hxx
        )

set(RPC
        rpc/MessageQueue.hxx
        rpc/Rendezvous.hxx
        )

add_executable(fibonacci
        ${LIB} ${RPC}
        FibonacciServer.hxx
        FibonacciClient.hxx
        fibonacci.cxx
        )
target_link_libraries(fibonacci pthread)
target_compile_options(fibonacci PRIVATE -Wall -Wextra -Werror -Wfatal-errors)
target_include_directories(fibonacci PRIVATE lib rpc)

