#pragma once

#include "Thread.hxx"
#include "MessageQueue.hxx"

namespace cppThreads {

    template<typename SendType, typename ReplyType>
    class RendezvousMessage {
        SendType                payload;
        MessageQueue<ReplyType> replyQ{0};

    public:
        explicit RendezvousMessage(SendType payload) : payload(payload) { }

        SendType  getPayload() const   { return payload; }
        void      reply(ReplyType msg) { replyQ.put(msg); }
        ReplyType waitForReply()       { return replyQ.get(); }
    };


    template<typename SendType, typename ReplyType>
    class RendezvousThread : public Thread {
        MessageQueue< RendezvousMessage<SendType,ReplyType>* >	inbox;

    protected:
        explicit RendezvousThread(size_t capacity = 0) : inbox{capacity} {}

        RendezvousMessage<SendType,ReplyType>* recv() { return inbox.get(); }

    public:
        ReplyType	send(SendType msg) {
            RendezvousMessage<SendType,ReplyType> wrapper{msg};
            inbox.put(&wrapper);
            return wrapper.waitForReply();
        }
    };
    
}
