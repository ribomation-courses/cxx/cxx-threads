#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include "FibonacciServer.hxx"
#include "FibonacciClient.hxx"
using namespace std;
using namespace cppThreads;


int main(int numArgs, char* args[]) {
    unsigned numTransactions = 10;
    unsigned numClients      = 3;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-t") {
            numTransactions = static_cast<unsigned int>(stoi(args[++k]));
        } else if (arg == "-c") {
            numClients = static_cast<unsigned int>(stoi(args[++k]));
        } else {
            cerr << "usage: " << args[0] << " [-t <num trans>] [-c <num clients>]\n";
            return 1;
        }
    }

    FibonacciServer fibSrv{numClients};

    vector<Client> clients;
    for (auto id = 1U; id<= numClients; ++id) {
        clients.emplace_back(id, numTransactions, fibSrv);
    }
    for (auto& c : clients) c.join();

    fibSrv.send(0);
    fibSrv.join();

    return 0;
}

