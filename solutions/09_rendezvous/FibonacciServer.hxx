#pragma once
#include <iostream>
#include <iomanip>
#include <sstream>
#include <chrono>
#include "Rendezvous.hxx"
using namespace std;
using namespace std::chrono;
using namespace cppThreads;


class FibonacciServer : public RendezvousThread<unsigned, unsigned long> {
public:
    explicit FibonacciServer(unsigned capacity = 0) : RendezvousThread{capacity} { start(); }

protected:
    void run() override {
        for (;;) {
            auto msg = recv();
            auto n   = msg->getPayload();
            if (n == 0) { msg->reply(0); return; }

            logArg(n);
            auto startTime = high_resolution_clock::now();
                auto result    = fib(n);
                msg->reply(result);
            auto endTime        = high_resolution_clock::now();
            auto elapsedSeconds = duration_cast<milliseconds>(endTime - startTime).count() * 10E-3;
            logRes(n, result, elapsedSeconds);
        }
    }

private:
    unsigned long fib(unsigned n) {
        return n <= 2 ? 1 : fib(n - 1) + fib(n - 2);
    }

    void logArg(unsigned n) {
        ostringstream buf;
        buf << "[server] REQ: n=" << n << "\n";
        cout << buf.str();
    }

    void logRes(unsigned n, unsigned long f, double elapsed) {
        ostringstream buf;
        buf << "[server] RES: fib(" << n << ") = " << f
            << " (elapsed " << elapsed << " secs.)\n";
        cout << buf.str();
    }
};

