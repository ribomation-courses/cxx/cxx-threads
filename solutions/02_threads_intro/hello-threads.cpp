#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <functional>
#include <pthread.h>

using namespace std;

unsigned numMessages = 100000;

void repeat(unsigned long n, const function<void(unsigned)>& f) {
    for (unsigned k = 0; k < n; ++k) f(k);
}

void* helloThread(void* arg) {
    const auto id   = reinterpret_cast<unsigned long>(arg);
    const string   name = "[" + to_string(id) + "] ";

    string   tab;
    repeat(id-1, [&](auto id){ tab += "     "; });

    cout << (tab + name + "started\n");
    repeat(numMessages, [&](auto k) {
        cout << (tab + name + "\n");
    });
    cout << (tab + name + "done\n");

    return nullptr;
}

int main(int numArgs, char* args[]) {
    unsigned numThreads = 10;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-m") {
            numMessages = static_cast<unsigned>(stoi(args[++k]));
        } else if (arg == "-t") {
            numThreads = static_cast<unsigned>(stoi(args[++k]));
        }
    }

    vector<pthread_t> threads;

    repeat(numThreads, [&](unsigned id) {
        pthread_t tid;
        auto arg = reinterpret_cast<void*>(id+1);
        pthread_create(&tid, nullptr, &helloThread, arg);
        threads.push_back(tid);
    });

    repeat(numThreads, [&](unsigned id) {
        pthread_join(threads[id], nullptr);
    });

    return 0;
}
