#pragma once
#include "Mutex.hxx"
#include "Guard.hxx"
#include "Condition.hxx"

namespace cppThreads {
    template<typename PayloadType=int>
    class Mailbox {
        PayloadType payload{};
        Mutex       mutex{};
        Condition   cond{mutex};
        bool        full{false};

    public:
        Mailbox() = default;
        ~Mailbox() = default;
        Mailbox(const Mailbox<PayloadType>&) = delete;
        Mailbox& operator=(const Mailbox<PayloadType>&) = delete;

        void put(PayloadType x) {
            /*
            Guard g(mutex);
            cond.wait([&]() { return !full; });

            payload = x;
            full = true;

            cond.notifyAll();
            */
            guarded(mutex, cond, [&] { return !full; }, [&] {
                payload = x;
                full    = true;
            });
        }

        PayloadType get() {
            /*
            Guard g(mutex);
            cond.wait([&]() { return full; });

            full = false;
            cond.notifyAll();

            return payload;
            */
            PayloadType x;
            guarded(mutex, cond, [&] { return full; }, [&] {
                x    = payload;
                full = false;
            });
            return x;
        }

        Mailbox<PayloadType>& operator<<(PayloadType x) {
            put(x);
            return *this;
        }

        Mailbox<PayloadType>& operator>>(PayloadType& x) {
            x = get();
            return *this;
        }
    };
}
