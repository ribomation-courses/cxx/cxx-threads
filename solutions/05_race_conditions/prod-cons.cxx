#include <iostream>
#include "Thread.hxx"
#include "Mailbox.hxx"
using namespace std;
using namespace cppThreads;

class Producer : public Thread {
    unsigned N;
    Mailbox<int>& out;
public:
    Producer(unsigned int N, Mailbox<int>& out) : N{N}, out{out} { start(); }

protected:
    void run() override {
        for (unsigned k = 1; k <= N; ++k) { out << k; }
        out << -1;
    }
};

class Consumer : public Thread {
    Mailbox<int>& in;
public:
    explicit Consumer(Mailbox<int>& in) : in(in) { start(); }

protected:
    void run() override {
        for (int msg; in >> msg, msg >= 0; ) {
            cout << "[consumer] " << msg << endl;
        }
    }
};

int main(int numArgs, char* args[]) {
    auto N = 100'000U;
    if (numArgs > 1) N = static_cast<unsigned int>(stoi(args[1]));
    
    Mailbox<int> mb;
    Consumer c{mb};
    Producer p{N, mb};
    p.join();
    c.join();

    return 0;
}
