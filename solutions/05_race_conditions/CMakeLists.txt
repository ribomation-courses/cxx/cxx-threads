cmake_minimum_required(VERSION 3.10)
project(05_race_conditions LANGUAGES CXX)

set(CMAKE_CXX_STANDARD          17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS        OFF)

add_executable(prod-cons
        Thread.hxx
        Mutex.hxx
        Guard.hxx
        Condition.hxx
        Mailbox.hxx
        prod-cons.cxx
        )
target_link_libraries (prod-cons pthread)
target_compile_options(prod-cons PRIVATE -Wall -Wextra -Werror -Wfatal-errors)

