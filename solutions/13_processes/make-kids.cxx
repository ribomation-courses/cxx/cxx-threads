#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "process.hxx"
extern "C" void exit(int);
using namespace std;

class Kid : public Process {
    unsigned id;
public:
    Kid(Kid&& that) noexcept : Process{move(that)}, id{that.id} {}
    explicit Kid(unsigned id) : id{id} { start(); }
    unsigned getId() const { return id; }
protected:
    int run() override {
        ostringstream buf;
        buf << "[kid-" << id << "] pid=" << getpid() << ", parent-pid=" << getppid() << "\n";
        cout << buf.str();

        //sleep(1);
        return static_cast<int>(id);
    }
};


int main(int argc, char* argv[]) {
    auto numKids  = (argc == 1) ? 100U : stoi(argv[1]);

    cout << "[parent] creating " << numKids << " child processes\n";
    vector<Kid> kids;
    for (auto   k = 1U; k <= numKids; ++k) { kids.emplace_back(k); }

    for (auto& k: kids) {
        k.join();
        cout << "[parent] done: child-" << k.getId() << ", pid=" << k.getPid() << endl;
    }
    return 0;
}
