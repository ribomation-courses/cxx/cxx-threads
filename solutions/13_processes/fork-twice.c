#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

void child() {
    printf("[child-%d] hello\n", getpid());
    sleep(3);
    printf("[child-%d] bye, bye\n", getpid());
    exit(0);
}

int main() {
    printf("[parent-%d] start\n", getpid());

    pid_t ch1 = fork();
    if (ch1 == 0) { child(); }

    pid_t ch2 = fork();
    if (ch2 == 0) { child(); }

    printf("[parent-%d] waiting for two child processes...\n", getpid());
    waitpid(ch1, NULL, 0);
    waitpid(ch2, NULL, 0);
    printf("[parent-%d] done\n", getpid());

    return 0;
}
