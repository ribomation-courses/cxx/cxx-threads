cmake_minimum_required(VERSION 3.10)
project(13_Processes LANGUAGES C CXX)

set(CMAKE_C_STANDARD 99)
set(CMAKE_CXX_STANDARD 17)

add_executable(fork-twice fork-twice.c)
target_compile_options(fork-twice PRIVATE -Wall -Wextra -Werror -Wfatal-errors)

add_executable(make-kids make-kids.cxx process.hxx)
target_compile_options(make-kids PRIVATE -Wall -Wextra -Werror -Wfatal-errors)

