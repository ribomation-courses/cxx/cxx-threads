#pragma once
#include <stdexcept>
#include <unistd.h>
#include <wait.h>
extern "C" void exit(int status);
using namespace std;

class Process {
    pid_t pid{};
protected:
    virtual int run() = 0;
public:
    Process(const Process&) = delete;
    Process& operator =(const Process&) = delete;
    Process(Process&& that) noexcept : pid{that.pid} {}

    Process() = default;
    virtual ~Process() { join(); }

    void start() {
        pid = fork();
        if (pid < 0) {
            throw runtime_error("cannot fork");
        } else if (pid == 0) {
            auto exitCode = run();
            exit(exitCode);
        }
    }

    int join() {
        int status;
        waitpid(pid, &status, 0);
        return WEXITSTATUS(status);
    }

    pid_t getPid() const { return pid; }
};
