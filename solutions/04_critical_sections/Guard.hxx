#pragma once
#include <pthread.h>
#include "Mutex.hxx"

namespace cppThreads {
    class Guard {
        Mutex& mutex;
        
    public:
        explicit Guard(Mutex& mutex) : mutex(mutex) {
            mutex.lock();
        }

        ~Guard() {
            mutex.unlock();
        }

        Guard(const Guard&)            = delete;
        Guard& operator=(const Guard&) = delete;
    };
}
