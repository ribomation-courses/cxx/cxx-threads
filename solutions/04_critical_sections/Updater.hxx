#pragma  once
#include "Thread.hxx"
#include "Account.hxx"

class Updater : public  cppThreads::Thread {
    unsigned  numTransactions;
    Account&  theAccount;

public:
    Updater(unsigned numTransactions, Account& theAccount)
            : numTransactions(numTransactions), theAccount(theAccount)
    {
        start();
    }

protected:
    void run() override {
        for (unsigned k = 0; k < numTransactions; ++k) {
            theAccount += +100;
        }

        for (unsigned k = 0; k < numTransactions; ++k) {
            theAccount += -100;
        }
    }
};

