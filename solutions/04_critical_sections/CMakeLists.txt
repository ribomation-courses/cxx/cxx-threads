cmake_minimum_required(VERSION 3.10)
project(04_critical_sections LANGUAGES CXX)

set(CMAKE_CXX_STANDARD          17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS        OFF)

add_executable(bank
        Thread.hxx
        Mutex.hxx
        Guard.hxx
        Account.hxx
        Updater.hxx
        bank.cxx
        )
target_link_libraries(bank pthread)
target_compile_options(bank PRIVATE -Wall -Wextra -Werror -Wfatal-errors)

