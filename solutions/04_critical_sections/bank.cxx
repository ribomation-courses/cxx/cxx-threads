#include <iostream>
#include <string>
#include <vector>
#include "Account.hxx"
#include "Updater.hxx"
using namespace std;
using namespace cppThreads;


int main(int numArgs, char* args[]) {
    int      initialBalance  = 0;
    unsigned numUpdaters     = 25;
    unsigned numTransactions = 1'000'000;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-u") {
            numUpdaters = static_cast<unsigned>( stoi(args[++k]));
        } else if (arg == "-t") {
            numTransactions = static_cast<unsigned>(stoi(args[++k]));
        } else if (arg == "-b") {
            initialBalance = stoi(args[++k]);
        } else {
            cerr << "usage: " << args[0] << " [-u <updaters>] [-t <transactions>]\n";
            return 1;
        }
    }

    cout.imbue(locale("en_US.UTF8"));
    cout << "Initial Balance: " << initialBalance << endl;
    cout << "# Updaters     : " << numUpdaters << endl;
    cout << "# Transactions : " << numTransactions << endl;

    Account theAccount{initialBalance};
    vector<Updater*> updaters;
    for (unsigned k = 0; k < numUpdaters; ++k) {
        updaters.push_back(new Updater(numTransactions, theAccount));
    }
    for (auto u : updaters) u->join();

    cout << "------------------------\n";
    cout << "Result: " << theAccount << endl;

    return 0;
}
