#pragma  once
#include "Guard.hxx"
#include "Mutex.hxx"
#include <vector>
#include <iostream>
#include "Account.hxx"

class Account {
    mutable cppThreads::Mutex mutex{};
    int balance = 0;

public:
    explicit Account(int balance) : balance{balance} {}

    int update(int amount) {
        cppThreads::Guard g(mutex);
        balance += amount;
        return balance;
    }

    int get() const {
        cppThreads::Guard g(mutex);
        return balance;
    }

    int operator +=(int amount) {
        return update(amount);
    }

    std::string toString() {
        return "Account{balance=" + std::to_string(get()) + "}";
    }

    friend std::ostream& operator <<(std::ostream& os, Account& a) {
        return os << a.toString();
    }

    Account() = default;
    ~Account() = default;
    Account(const Account&) = delete;
    Account& operator =(const Account&) = delete;
};

