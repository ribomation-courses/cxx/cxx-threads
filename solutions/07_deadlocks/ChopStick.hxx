#pragma once

#include "Mutex.hxx"
#include "Condition.hxx"
#include "Guard.hxx"

using namespace cppThreads;

class ChopStick {
    Mutex     mutex{};
    Condition notBusy{mutex};
    bool      busy = false;

public:
    ChopStick() = default;
    ~ChopStick() = default;
    ChopStick(const ChopStick&) = delete;
    ChopStick& operator=(const ChopStick&) = delete;

    void acquire() {
        Guard g(mutex);
        notBusy.waitUntil([&]() { return !busy; });
        busy = true;
    }

    void release() {
        Guard g(mutex);
        busy = false;
        notBusy.notifyAll();
    }
};


