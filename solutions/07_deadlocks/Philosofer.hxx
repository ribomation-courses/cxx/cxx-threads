#pragma  once

#include <iostream>
#include <sstream>
#include <string>
#include <random>
#include <unistd.h>
#include "Thread.hxx"
#include "ChopStick.hxx"

using namespace std;
using namespace cppThreads;


class Philosofer : public Thread {
    unsigned id;
    ChopStick* left;
    ChopStick* right;
    bool running = true;

public:
    Philosofer(unsigned id, ChopStick* left, ChopStick* right)
            : id(id), left(left), right(right) {
        start();
    }

protected:
    void run() {
        random_device                      R{};
        default_random_engine              r{R()};
        uniform_int_distribution<unsigned> nextSleepTime(10 * 1000, 300 * 1000);

        while (running) {
            log("THINKING");
            //usleep(nextSleepTime(r));

            log("WAIT LEFT");
            left->acquire();
            log("WAIT RIGHT");
            right->acquire();

            log("EATING");
            usleep(nextSleepTime(r));

            right->release();
            left->release();
        }
    }

private:
    void log(const char* msg) {
        ostringstream buf;
        buf << string(id * 15, ' ') << "P" << (id + 1) << ":" << msg << "\n";
        cout << buf.str();
    }
};
