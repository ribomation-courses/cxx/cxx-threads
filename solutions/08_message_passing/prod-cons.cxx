#include <iostream>
#include "Thread.hxx"
#include "MessageQueue.hxx"

using namespace std;
using namespace cppThreads;


class Producer : public Thread {
    int N;
    MessageQueue<int>& out;

public:
    Producer(int N, MessageQueue<int>& out)
            : N(N), out(out) {
        start();
    }

protected:
    void run() override {
        for (int k = 1; k <= N; ++k) {
            out.put(k);
        }
        out.put(-1);
    }
};


class Consumer : public Thread {
    MessageQueue<int>& in;

public:
    explicit Consumer(MessageQueue<int>& in) : in(in) {
        start();
    }

protected:
    void run() override {
        for (int n = 0; (n = in.get()) >= 0;) {
            cout << "[consumer] " << n << endl;
        }
    }
};


int main(int numArgs, char* args[]) {
    auto N = (numArgs > 1) ? stoi(args[1]) : 42;

    MessageQueue<int> q{8};
    Consumer          c{q};
    Producer          p{N, q};
    p.join();
    c.join();

    return 0;
}
