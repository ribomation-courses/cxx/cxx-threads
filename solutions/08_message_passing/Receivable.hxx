
#pragma once

namespace cppThreads {
    
    template<typename MessageType>
    struct Receivable {
        virtual void send(MessageType) = 0;
    };
    
}
