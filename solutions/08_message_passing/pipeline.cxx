#include <iostream>
#include <string>

#include "Thread.hxx"
#include "Receivable.hxx"
#include "MessageThread.hxx"

using namespace std;
using namespace cppThreads;

using XXL = unsigned long long;


class Producer : public Thread {
    XXL N;
    Receivable<XXL>* next;

public:
    Producer(XXL N, Receivable<XXL>* next) : N{N}, next{next} {
        start();
    }

protected:
    void run() override {
        for (auto k = 1ULL; k <= N; ++k) { next->send(k); }
        next->send(0);
    }
};

class Transformer : public MessageThread<XXL> {
    Receivable<XXL>* next;

public:
    explicit Transformer(Receivable<XXL>* next) : next{next} { start(); }

protected:
    void run() override {
        for (auto n = recv(); n != 0; n = recv()) {
            next->send(42 + n);
        }
        next->send(0);
    }
};


class Consumer : public MessageThread<XXL> {
public:
    Consumer() { start(); }

protected:
    void run() override {
        for (auto n = recv(); n != 0; n = recv()) {
            cout << "[consumer] " << n << endl;
        }
    }
};


int main(int numArgs, char* args[]) {
    auto numTransformers = 5U;
    auto numMessages     = 10U;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-t") {
            numTransformers = static_cast<unsigned>(stoi(args[++k]));
        } else if (arg == "-m") {
            numMessages = static_cast<unsigned>(stoi(args[++k]));
        } else {
            cerr << "usage: " << args[0] << " [-t <num trans>] [-m <num msgs>]\n";
            return 1;
        }
    }

    Consumer c;
    Receivable<XXL>* next = &c;
    while (numTransformers-- > 0) { next = new Transformer{next}; }
    Producer p{numMessages, next};

    p.join();
    c.join();

    return 0;
}

