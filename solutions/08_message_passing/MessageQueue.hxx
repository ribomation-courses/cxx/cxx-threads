#pragma once
#include <queue>
#include "Mutex.hxx"
#include "Condition.hxx"
#include "Guard.hxx"


namespace cppThreads {

    template<typename MessageType>
    class MessageQueue {
        std::queue<MessageType> inbox;
        const size_t            capacity;
        Mutex                   lock;
        Condition               notEmpty{lock};
        Condition               notFull{lock};

        bool   isBounded() const { return capacity > 0; }
        size_t size()      const { return inbox.size(); }
        bool   empty()     const { return inbox.empty(); }
        bool   full()      const { return isBounded() && size() >= capacity; }

    public:
        explicit MessageQueue(size_t capacity = 0) : capacity{capacity} {}

        void put(MessageType msg) {
            Guard g(lock);
            notFull.waitUntil([this]() { return !full(); });

            inbox.push(msg);

            notEmpty.notifyAll();
        }

        MessageType get() {
            Guard g(lock);
            notEmpty.waitUntil([this]() { return !empty(); });

            MessageType msg = inbox.front();
            inbox.pop();

            notFull.notifyAll();
            return msg;
        }
    };

}
