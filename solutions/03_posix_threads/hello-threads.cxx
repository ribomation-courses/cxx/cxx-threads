#include <iostream>
#include <sstream>
#include <vector>
#include "Thread.hxx"

using namespace std;
using namespace cppThreads;

class Hello : public Thread {
    const string   name;
    const unsigned N;

    static string makeName(unsigned id) {
        ostringstream buf;
        buf << string((id - 1) * 4, ' ') << "[" << id << "] ";
        return buf.str();
    }

public:
    Hello(unsigned id, unsigned N) : name{makeName(id)}, N{N} {
        start();
    }

protected:
    void run() override {
        cout << (name + "started\n");
        for (unsigned k = 1; k <= N; ++k) cout << (name + "\n");
        cout << (name + "done\n");
    }
};

int main(int numArgs, char* args[]) {
    auto numThreads  = 10U;
    auto numMessages = 10'000U;

    for (auto k       = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-m") {
            numMessages = static_cast<unsigned>(stoi(args[++k]));
        } else if (arg == "-t") {
            numThreads = static_cast<unsigned>(stoi(args[++k]));
        }
    }

    vector<Hello*> threads;
    for (auto      id = 1U; id <= numThreads; ++id) {
        threads.push_back(new Hello(id, numMessages));
    }
    for (auto      t : threads) t->join();
    for (auto      t : threads) delete t;

    return 0;
}
