#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <thread>
#include <cstdlib>

using namespace std;

int main(int numArgs, char* args[]) {
    auto numThreads  = 20U;
    auto numMessages = 10'000U;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-t") {
            numThreads = static_cast<unsigned int>(stoi(args[++k]));
        } else if (arg == "-m") {
            numMessages = static_cast<unsigned int>(stoi(args[++k]));
        } else {
            cerr << "usage: " << args[0] << " [-t <int>] [-m <int>]" << endl;
            ::exit(1);
        }
    }

    auto run  = [=](unsigned id) {
        for (auto k = 0U; k < numMessages; ++k) {
            ostringstream buf;
            buf << string(id * 4, ' ') << id << "\n";
            cout << buf.str();
        }
    };

    vector<thread> threads;
    for (auto      k = 0U; k < numThreads; ++k)
        threads.push_back( thread{run, k + 1} );

    for (auto& t : threads) t.join();

    return 0;
}
