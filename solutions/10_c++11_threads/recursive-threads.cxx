#include <iostream>
#include <sstream>
#include <thread>
using namespace std;

void launch(const unsigned N, const unsigned M) {
    thread t{
            [=]() {
                for (auto k = 1U; k <= M; ++k) {
                    ostringstream buf;
                    buf << string((N - 1) * 4, ' ') << N << "\n";
                    cout << buf.str();
                }
            }
    };
    if (N > 1) launch(N - 1, M);
    t.join();
}

int main(int numArgs, char* args[]) {
    auto numThreads  = 20U;
    auto numMessages = 10'000U;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-t") {
            numThreads = static_cast<unsigned int>(stoi(args[++k]));
        } else if (arg == "-m") {
            numMessages = static_cast<unsigned int>(stoi(args[++k]));
        } else {
            cerr << "usage: " << args[0] << " [-t <int>] [-m <int>]" << endl;
            return 1;
        }
    }

    launch(numThreads, numMessages);
    return 0;
}
