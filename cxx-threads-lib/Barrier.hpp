#pragma once

#include <pthread.h>

namespace cppThreads {
    class Barrier {
        pthread_barrier_t barrier;
    public:
        Barrier(int numWorkers) {
            pthread_barrier_init(&barrier, NULL, numWorkers);
        }
        
         ~Barrier() {
             pthread_barrier_destroy(&barrier);
         }
        
        void wait() {
            pthread_barrier_wait(&barrier);
        }
    };
}

