#pragma once

#include "Thread.hpp"
#include "MessageQueue.hpp"

namespace cppThreads {

    template<typename SendType, typename ReplyType>
    class RendezvousMessage {
        SendType                payload;
        MessageQueue<ReplyType> replyQ{0};

    public:
        RendezvousMessage(SendType payload) : payload(payload) { }

        SendType  getPayload() const   { return payload; }
        void      reply(ReplyType msg) { replyQ.put(msg); }
        ReplyType waitForReply()       { return replyQ.get(); }
    };


    template<typename SendType, typename ReplyType>
    class RendezvousThread : public Thread {
        MessageQueue< RendezvousMessage<SendType,ReplyType>* >	q{0};

    protected:
        RendezvousMessage<SendType,ReplyType>* recv() { return q.get(); }

    public:
        ReplyType	send(SendType msg) {
            RendezvousMessage<SendType,ReplyType>* m = new RendezvousMessage<SendType,ReplyType>(msg);
            q.put(m);
            ReplyType r = m->waitForReply();
            delete m;
            return r;
        }
    };
    
}
