#pragma once

#include <pthread.h>
#include "Mutex.hpp"

namespace cppThreads {
    class Guard {
        Mutex& mutex;
        
    public:
        Guard(Mutex& mutex) : mutex(mutex) {
            mutex.lock();
        }

        ~Guard() {
            mutex.unlock();
        }

        Guard(const Guard&) = delete;
        Guard& operator=(const Guard&) = delete;
    };
}
