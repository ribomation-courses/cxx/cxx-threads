#pragma once

#include <queue>
#include "Mutex.hpp"
#include "Condition.hpp"
#include "Guard.hpp"


namespace cppThreads {
    template<typename MessageType>
    class MessageQueue {
        std::queue<MessageType> inbox;
        size_t                  capacity;
        Mutex                   lock;
        Condition               notEmpty;
        Condition               notFull;

    public:
        MessageQueue(size_t capacity = 0)
                : capacity(capacity), notEmpty(lock), notFull(lock) { }

        bool   isBounded() const { return capacity > 0; }
        size_t size()      const { return inbox.size(); }
        bool   isEmpty()   const { return inbox.empty(); }
        bool   isFull()    const { return isBounded() && size() >= capacity; }

        
        void put(MessageType msg) {
            Guard g(lock);
            notFull.wait([this]() { return !isFull(); });
            
            inbox.push(msg);
            
            notEmpty.notifyAll();
        }

        MessageType get() {
            Guard g(lock);
            notEmpty.wait([this](){return !isEmpty();});
            
            MessageType msg = inbox.front();
            inbox.pop();
            
            notFull.notifyAll();
            return msg;
        }
    };
}
