#pragma once

#include "Thread.hpp"
#include "MessageQueue.hpp"

namespace cppThreads {
    template<typename MessageType>
    class MessageThread : public Thread, public Receivable<MessageType> {
        MessageQueue<MessageType> q;

    protected:
        MessageThread(size_t capacity = 0) : q{capacity} { }

        MessageType recv() { return q.get(); }

    public:
        void send(MessageType x) { q.put(x); }
    };
}

