#pragma once
#include <pthread.h>

namespace cppThreads {
    class Mutex {
        pthread_mutex_t mutex;

    public:
        Mutex(bool recursive = true) {
            pthread_mutexattr_t attrs;
            pthread_mutexattr_init(&attrs);

            pthread_mutexattr_settype(&attrs, recursive ? PTHREAD_MUTEX_RECURSIVE : PTHREAD_MUTEX_DEFAULT);
            pthread_mutex_init(&mutex, &attrs);

            pthread_mutexattr_destroy(&attrs);
        }

        ~Mutex() {
            pthread_mutex_destroy(&mutex);
        }

        Mutex(const Mutex&) = delete;
        Mutex& operator=(const Mutex&) = delete;
        

        void lock() {
            pthread_mutex_lock(&mutex);
        }

        void unlock() {
            pthread_mutex_unlock(&mutex);
        }

        pthread_mutex_t* native() {
            return &mutex;
        }
    };
}
