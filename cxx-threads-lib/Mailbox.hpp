#pragma once

#include "Mutex.hpp"
#include "Guard.hpp"
#include "Condition.hpp"

namespace cppThreads {
    template<typename PayloadType=int>
    class Mailbox {
        PayloadType payload{};
        Mutex       mutex{};
        Condition   cond{mutex};
        bool        full = false;

    public:
        Mailbox() = default;
        ~Mailbox() = default;
        Mailbox(const Mailbox&) = delete;
        Mailbox& operator=(const Mailbox&) = delete;

        void put(PayloadType x) {
            Guard g(mutex);
            cond.wait([&]() { return !full; });
            payload = x;
            full = true;
            cond.notifyAll();
        }

        PayloadType get() {
            Guard g(mutex);
            cond.wait([&]() { return full; });
            full = false;
            cond.notifyAll();
            return payload;
        }
    };
}
